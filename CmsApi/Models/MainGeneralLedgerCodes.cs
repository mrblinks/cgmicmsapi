﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CmsApi.Models
{
    public class MainGeneralLedgerCodes 
    {
        [Key]
        public int MainGeneralLedgerCodeId { get; set; }
        [StringLength(20)]
        [Required]
        public string Code { get; set; }
        [StringLength(100)]
        [Required]
        public string Description { get; set; }
        public int CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}