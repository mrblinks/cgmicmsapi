﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CmsApi.Models
{
    public class Member : BaseModel
    {
        [Key]
        public int MemberId { get; set; }
        public string MemberNumber { get; set; }
        [Required]
        public string FullName { get; set; }
        public string Gender { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        [Required]
        public string MaritalStatus { get; set; }
        public string Picture { get; set; }
        [Required]
        public int GroupId { get; set; }
        [Required]
        public int DepartmentId { get; set; }
        public string Status { get; set; }
        public Groups Group { get; set; }
        public Department Department { get; set; }
        
        public BranchDetails Branch { get; set; }
        public CompanyDetails Company { get; set; }
    }
}