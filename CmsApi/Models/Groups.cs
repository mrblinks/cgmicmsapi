﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CmsApi.Models
{
    public class Groups : BaseModel
    {
        [Key]
        public int GroupId { get; set; }
        [StringLength(50)]
        public string GroupName { get; set; }

        public CompanyDetails CompanyDetails { get; set; }
        public BranchDetails Branch { get; set; }
    }
}
