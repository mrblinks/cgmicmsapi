﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class Message : BaseModel
    {
        [Key]
        public int MessageId { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Messages { get; set; }

        public BranchDetails Branch { get; set; }
        public CompanyDetails Company { get; set; }
    }
}
