﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class AcctTransactions : BaseModel
    {
        [Key]
        public int AcctTransactionId { get; set; }
        public int GeneralLedgerCodeId { get; set; }
        public int GlCodeId { get; set; }
        public int TransactionId { get; set; }

        public GeneralLedgerCode GeneralLedgerCode { get; set; }
        public Transaction Transaction { get; set; }
    }
}
