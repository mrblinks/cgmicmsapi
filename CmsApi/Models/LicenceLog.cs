﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class LicenceLog
    {
        [Key]
        public int LicenceLogid { get; set; }
        public DateTime? Previous { get; set; }
        [Required]
        public DateTime Next { get; set; }
        [Required]
        public int CreatedUserId { get; set; }
        [Required]
        public int BranchId { get; set; }
        public int CompanyId { get; set; }
        public DateTime CreatedDate { get; set; }

        public BranchDetails Branch { get; set; }
        public CompanyDetails Company { get; set; }
    }
}
