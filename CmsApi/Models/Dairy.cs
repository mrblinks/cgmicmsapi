﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class Dairy : BaseModel
    {
        [Key]
        public int DairyId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public string Status { get; set; }

        public CompanyDetails CompanyDetails { get; set; }
        public BranchDetails Branch { get; set; }
    }
}
