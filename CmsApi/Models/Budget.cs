﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class Budget : BaseModel
    {
        [Key]
        public int BudgetId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public string Year { get; set; }
        public string Status { get; set; }
        public Currency Currency { get; set; }
        public BranchDetails Branch { get; set; }
        public CompanyDetails Company { get; set; }
    }
}
