﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class Events : BaseModel
    {
        [Key]
        public int EventId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime EventDate { get; set; }

        public string Status { get; set; }

        public BranchDetails Branch { get; set; }
        public CompanyDetails Company { get; set; }
    }
}
