﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CmsApi.Models
{
    public class TransCodeItems : BaseModel
    {
        public int TransCodeItemsId { get; set; }
        [StringLength(10)]
        public string LedgerType { get; set; }
        [StringLength(50)]
        public string TransCode { get; set; }
        [StringLength(100)]
        public string Reference { get; set; }
        [Column(TypeName = "Money")]
        public decimal? Amount { get; set; }
        [StringLength(50)]
        public string TransSource { get; set; }
        public string Status { get; set; }
        public int? MemberId { get; set; }
        public int GeneralLegderCodeId { get; set; }
    }
}