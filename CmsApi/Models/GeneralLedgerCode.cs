﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CmsApi.Models
{
    public class GeneralLedgerCode : BaseModel
    {
        [Key]
        public int  GeneralLedgerCodeId { get; set; }
        [Required]
        public string GlType { get; set; }
        [Required]
        public string SubCode { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string BalanceType { get; set; }
        public decimal Balance { get; set; }
        [Required]
        public int MainGeneralLedgerCodeId { get; set; }
        [Required]
        public int CurrencyId { get; set; }

        public Currency Currency { get; set; }
        public MainGeneralLedgerCodes MainGeneralLedgerCodes { get; set; }

        public CompanyDetails CompanyDetails { get; set; }
        public BranchDetails Branch { get; set; }

    }
}