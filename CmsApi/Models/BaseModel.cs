﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class BaseModel
    {

        [Required]
        public int CreatedUserId { get; set; }
        [Required]
        public int BranchId { get; set; }
        [Required]
        public int CompanyId { get; set; }
        public DateTime CreatedDate { get; set; }
        
    }
}
