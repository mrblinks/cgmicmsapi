﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CmsApi.Models
{
    public class PaymentMethod
    {
        [Key]
        public int PaymentMethodId { get; set; }
        [StringLength(50)]
        [Required]
        public string MethodName { get; set; }

        public int CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
