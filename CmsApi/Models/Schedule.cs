﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class Schedule : BaseModel
    {
        [Key]
        public int ScheduleId { get; set; }
        public string Destination { get; set; }
        [Required]
        public string Message { get; set; }
        public string Status { get; set; }
        [Required]
        public DateTime Date { get; set; }

        public BranchDetails Branch { get; set; }
        public CompanyDetails Company { get; set; }
    }
}
