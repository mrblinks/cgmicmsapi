﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class ChurchServices : BaseModel
    {
        [Key]
        public int ChurchServicesId { get; set; }
        [Required]
        public string Name { get; set; }

        public BranchDetails Branch { get; set; }
        public CompanyDetails CompanyDetails { get; set; }
    }
}
