﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class Attendance : BaseModel
    {
        [Key]
        public int AttendanceId { get; set; }
        [Required]
        public int ChurchServicesId { get; set; }
        [Required]
        public int Men { get; set; }
        [Required]
        public int Women { get; set; }
        [Required]
        public int Children { get; set; }
        public string Description { get; set; }
        public ChurchServices ChurchServices { get; set; }

        public CompanyDetails Company { get; set; }
        public BranchDetails Branch { get; set; }
    }
}
