﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CmsApi.Models
{
    public class CompanyDetails
    {
        [Key]
        public int CompanyId { get; set; }
        public string CompanyNo { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string Postal { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Logo { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}