﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class ReceiptTransactions : BaseModel
    {
        [Key]
        public int ReceiptTransactionId { get; set; }
        public int GeneralLedgerCodeId { get; set; }
        public int TransactionId { get; set; }

        public GeneralLedgerCode GeneralLedgerCode { get; set; }
        public Transaction Transaction { get; set; }
    }
}
