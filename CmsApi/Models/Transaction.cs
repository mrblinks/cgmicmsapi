﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace CmsApi.Models
{
    public class Transaction : BaseModel
    {
        [Key]
        public int TransactionId { get; set; }
        public string TransCode { get; set; }
        [Required]
        public int GeneralLedgerCodeId { get; set; }
        [NotMapped]
        public int MemberId { get; set; }
        [NotMapped]
        public int AccountId { get; set; }
        [Required]
        public int PaymentMethodId { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public string TransSource { get; set; }
        public string TransType { get; set; }
        [Required]
        [Column(TypeName = "Money")]
        public decimal Amount { get; set; }
        [Required]
        public string Reference { get; set; }

        //public Account Account { get; set; }
        //public Member Member { get; set; }
        public Currency Currency { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public GeneralLedgerCode GetGeneralLedgerCode { get; set; }

        public CompanyDetails CompanyDetails { get; set; }
        public BranchDetails Branch { get; set; }

    }
}