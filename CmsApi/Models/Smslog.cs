﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class Smslog
    {
        [Key]
        public int SmslogId { get; set; }
        public string Destination { get; set; }
        public string Message { get; set; }
        public string Response { get; set; }
        public int? ScheduleId { get; set; }
        public int? CreatedUserId { get; set; }
        public int BranchId { get; set; }
        public int CompanyId { get; set; }
        public DateTime CreatedDate { get; set; }

        public Schedule Schedule { get; set; }
        public BranchDetails Branch { get; set; }
        public CompanyDetails Company { get; set; }
    }
}
