﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CmsApi.Models
{
    public class Currency
    {
        [Key]
        public int CurrencyId { get; set; }

        [StringLength(20)]
        [Required]
        public string TypeName { get; set; }
        [StringLength(10)]
        [Required]
        public string Symbol { get; set; }

        public int CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
