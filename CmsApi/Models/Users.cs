﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CmsApi.Models
{
    public class Users
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        [StringLength(200)]
        public string Email { get; set; }
        public string Phone { get; set; }
        [Required]
        public string UserType { get; set; }
        public string Image { get; set; }
        [Required]
        public string Username { get; set; }
        [NotMapped]
        public string Password { get; set; }
        public string Privillege { get; set; }
        public string Status { get; set; }

        public DateTime? LastLogin { get; set; }
        public DateTime? LastLogOut { get; set; }
        [Required]
        public int CreatedUserId { get; set; }
        [Required]
        public int BranchId { get; set; }
        [Required]
        public int CompanyId { get; set; }
        public DateTime CreatedDate { get; set; }


        public CompanyDetails CompanyDetails { get; set; }
        public BranchDetails Branch { get; set; }

    }
}
