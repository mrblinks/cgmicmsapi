﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CmsApi.Models
{
    public class Department : BaseModel
    {
        [Key]
        public int DepartmentId { get; set; }
        [StringLength(20)]
        [Required]
        public string DptName { get; set; }

        public CompanyDetails CompanyDetails { get; set; }
        public BranchDetails Branch { get; set; }
    }
}
