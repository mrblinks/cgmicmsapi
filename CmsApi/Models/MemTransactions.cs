﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class MemTransactions : BaseModel
    {
        [Key]
        public int MemTransactionsId { get; set; }
        public int MemberId { get; set; }
        public int TransactionId { get; set; }

        public Member Member { get; set; }
        public Transaction Transaction { get; set; }
    }
}
