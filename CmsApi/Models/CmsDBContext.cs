﻿using Microsoft.EntityFrameworkCore;
using CmsApi.Models;

namespace CmsApi.Models
{
    public class CmsDbContext : DbContext
    {
        public CmsDbContext(DbContextOptions<CmsDbContext> options) : base(options)
        {
           
        }

        public DbSet<Sequence> Sequences { get; set; }
        public DbSet<CompanyDetails> CompanyDetails { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<BranchDetails> BranchDetails { get; set; }
        public DbSet<Groups> Groups { get; set; }
        public DbSet<Currency> PaymentType { get; set; }
        public DbSet<Todo> Todo { get; set; }
        public DbSet<Smslog> Smslog { get; set; }
        public DbSet<SmsApi> SmsApi { get; set; }
        public DbSet<Events> Events { get; set; }
        public DbSet<Dairy> Dairy { get; set; }
        public DbSet<Budget> Budget { get; set; }
        public DbSet<LicenceLog> LicenceLog { get; set; }
        public DbSet<Message> Message { get; set; }
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<ChurchServices> ChurchServices { get; set; }
        public DbSet<Attendance> Attendance { get; set; }
        public DbSet<PaymentMethod> PaymentMethod { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<MemTransactions> MenTransactions { get; set; }
        public DbSet<AcctTransactions> AcctTransactions { get; set; }
        public DbSet<ReceiptTransactions> ReceiptTransactions { get; set; }
        public DbSet<GeneralLedgerCode> GeneralLedgerCodes { get; set; }
        public DbSet<MainGeneralLedgerCodes> MainGeneralLedgerCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
        
    }
}