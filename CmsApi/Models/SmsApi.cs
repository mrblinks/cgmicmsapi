﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi.Models
{
    public class SmsApi : BaseModel
    {
        [Key]
        public int SmsApiId { get; set; }
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(200)]
        [Required]
        public string Url { get; set; }
        [StringLength(50)]
        [Required]
        public string Username { get; set; }
        [StringLength(50)]
        [Required]
        public string Password { get; set; }
        [StringLength(50)]
        [Required]
        public string SenderId { get; set; }
        [Required]
        public bool Default { get; set; }
        [Required]
        public string Status { get; set; }
        
        public CompanyDetails CompanyDetails { get; set; }
        public BranchDetails Branch { get; set; }
    }
}
