﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CmsApi.Models
{
    public class BranchDetails 
    {
        [Key]
        public int BranchId { get; set; }
        [Required]
        public int CompanyId { get; set; }
        [Required]
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        [Required]
        public string Postal { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Telephone { get; set; }
        [Required]
        public string Region { get; set; }
        public string District { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Status { get; set; }
        [Required]
        public int CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }

        public CompanyDetails CompanyDetails { get; set; }

    }
}