﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CmsApi.Models;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace CmsApi.MyServices
{
    public class SequenceCode
    {
        private readonly CmsDbContext _context;

       public SequenceCode(CmsDbContext context)
        {
            _context = context;
        }

        
        public async Task<string> GetCode(string type)
        {
            Sequence sequence = await _context.Sequences.FirstOrDefaultAsync(a => a.Name.ToLower() == type.ToLower());

            if (sequence != null)
            {
                //string code = sequence.Prefix + sequence.Suffix + sequence.Counter;
                //int codeint = Convert.ToInt32(sequence.Counter); codeint++;

                string transcodenum = (sequence.Counter).ToString();
                int length = sequence.Length;
                int padnum = length - transcodenum.Length;
                string number = transcodenum.PadLeft(padnum + 1, '0');
                string code = sequence.Prefix + number + sequence.Suffix;

                sequence.Counter += 1;
                _context.Entry(sequence).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return code;
            }
            return null;
        }

        public async Task<string> Sms(Member member)
        {
            CompanyDetails church = _context.CompanyDetails.Where(a => a.CompanyId == member.CompanyId).LastOrDefault();
            BranchDetails branch = _context.BranchDetails.Where(a => a.BranchId == member.BranchId).LastOrDefault();
            SmsApi smsconfig = _context.SmsApi.LastOrDefault(a => a.Status.ToLower().Contains("active") && a.BranchId == member.BranchId);
            Message msg = _context.Message.LastOrDefault(a => a.Type.ToLower().Contains("welcome") && a.BranchId == member.BranchId);
            if (smsconfig == null)
            {
                return null;
            }
            else
            {
                Smslog smslog = new Smslog();
                smslog.CreatedUserId = member.CreatedUserId; smslog.BranchId = member.BranchId;
                smslog.CompanyId = member.CompanyId; smslog.CreatedDate = DateTime.UtcNow;
                SmsModel model = new SmsModel();
                model.Destination = member.Mobile;
                if (model.Message == null)
                {
                    model.Message = $"You are Welcome to {church.CompanyName}, {branch.BranchName}. We are Glad {member.FullName} to have you. your Member Number is {member.MemberNumber} ";
                }
                model.Message = msg.Messages;
                smslog.Destination = model.Destination; smslog.Message = model.Message;
                
                try
                {
                    var httpClient = new HttpClient();
                    StringBuilder sb = new StringBuilder();

                    sb.Append(smsconfig.Url).Append("?cmd=sendquickmsg&owneremail=Harmonizerblinks@gmail.com&subacct=")
                        .Append(smsconfig.Username).Append("&subacctpwd=").Append(smsconfig.Password)
                        .Append("&message=").Append(model.Message).Append("&sender=").Append(smsconfig.SenderId)
                        .Append("&sendto=").Append(model.Destination).Append("&msgtype=0");
                    var json = await httpClient.GetStringAsync(sb.ToString());
                    //var smsresponse = JsonConvert.DeserializeObject<SmsResponse>(json);
                    smslog.Response = json;
                    _context.Smslog.Add(smslog);
                    await _context.SaveChangesAsync();

                    return json;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }


        public async Task<string> SendMessage(Member member)
        {
            SmsApi smsconfig = _context.SmsApi.Where(a => a.BranchId == member.BranchId && a.Status.ToLower().Contains("active")).FirstOrDefault();
            var Message = $"Welcome to {member.CompanyId} you have been Successfull Added to our Member Database";

            var httpClient = new HttpClient();
            StringBuilder sb = new StringBuilder();
            sb.Append(smsconfig.Url).Append("?&username=").Append(smsconfig.Username)
                    .Append("&password=").Append(smsconfig.Password).Append("&source=").Append(smsconfig.SenderId);
            sb.Append("&destination=").Append(member.Mobile).Append("&message=").Append(Message);
            var json = await httpClient.GetStringAsync(sb.ToString());
            var smsresponse = JsonConvert.DeserializeObject<SmsResponse>(json);

            return smsresponse.Message;
        }


        public class SmsModel
        {
            public string Destination { get; set; }
            public string Message { get; set; }
        }

        public class SmsResponse
        {
            public int Code { get; set; }
            public string Message { get; set; }
        }
    }
}
