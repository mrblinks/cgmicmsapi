﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using CmsApi.Services;

namespace CmsApi.MyServices
{
    public class EmailSender : IEmailSender
    {
        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; } //set only via Secret Manager

        public Task SendEmailAsync(string email, string subject, string message)
        {
            string FromEmail = "noreply@Acyst.Tech"; 
            return Execute(FromEmail, subject, message, email);
        }

        public  Task Execute(string fromEmail, string subject, string message, string email)
        {
            SmtpClient client = new SmtpClient();
            //client.Host = "bh-71.webhostbox.net";
            //client.Port = 465;
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            //client.Credentials = new NetworkCredential("ict@cgmibzv.com".Trim(), "Cgmibzv@1".Trim());
            client.Credentials = new NetworkCredential("emailaddress@gmail.com".Trim(), "pasword@12".Trim());

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(fromEmail);
            mailMessage.To.Add(email);
            mailMessage.Body = message;
            mailMessage.Subject = subject;
            mailMessage.IsBodyHtml = true;
            return  client.SendMailAsync(mailMessage);
        }

    }
}
