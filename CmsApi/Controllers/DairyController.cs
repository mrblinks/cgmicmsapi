﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Dairy")]
    public class DairyController : Controller
    {
        private readonly CmsDbContext _context;

        public DairyController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/Dairy
        [HttpGet]
        public IEnumerable<Dairy> GetDairy()
        {
            return _context.Dairy.Include(a => a.Branch).Include(a => a.CompanyDetails);
        }

        // GET: api/Dairy/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDairy([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dairy = await _context.Dairy.SingleOrDefaultAsync(m => m.DairyId == id);

            if (dairy == null)
            {
                return NotFound();
            }

            return Ok(dairy);
        }

        // GET: api/Dairy/User/5
        [HttpGet("User/{id}")]
        public async Task<IActionResult> GetUserDairy([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var user = await _context.Users.SingleOrDefaultAsync(m => m.UserId == id);
            if (user == null) return NotFound($"User With id {id} Does not Exist");

            var dairy = _context.Dairy.Where(m => m.CreatedUserId == id);

            if (dairy == null)
            {
                return NotFound();
            }

            return Ok(dairy);
        }

        // PUT: api/Dairy/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDairy([FromRoute] int id, [FromBody] Dairy dairy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dairy.DairyId)
            {
                return BadRequest();
            }

            _context.Entry(dairy).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DairyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Dairy
        [HttpPost]
        public async Task<IActionResult> PostDairy([FromBody] Dairy dairy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Dairy.Add(dairy);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDairy", new { id = dairy.DairyId }, dairy);
        }

        // DELETE: api/Dairy/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDairy([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dairy = await _context.Dairy.SingleOrDefaultAsync(m => m.DairyId == id);
            if (dairy == null)
            {
                return NotFound();
            }

            _context.Dairy.Remove(dairy);
            await _context.SaveChangesAsync();

            return Ok(dairy);
        }

        private bool DairyExists(int id)
        {
            return _context.Dairy.Any(e => e.DairyId == id);
        }
    }
}