﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;
using Microsoft.AspNetCore.Authorization;
using CmsApi.MyServices;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/BranchDetails")]
    public class BranchDetailsController : Controller
    {
        private readonly CmsDbContext _context;
        SequenceCode seq;

        public BranchDetailsController(CmsDbContext context)
        {
            _context = context; 
            seq = new SequenceCode(context);
        }

        // GET: api/BranchDetails
        [HttpGet]
        public IEnumerable<BranchDetails> GetBranchDetails()
        {
            return _context.BranchDetails.Include(c=>c.CompanyDetails);
        }

        // GET: api/BranchDetails/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBranchDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var branchDetails = _context.BranchDetails.Where(m => m.BranchId == id).Include(a=>a.CompanyDetails).FirstOrDefault();

            if (branchDetails == null)
            {
                return NotFound();
            }

            return Ok(branchDetails);
        }

        // GET: api/BranchDetails/Company/5
        [HttpGet("Church/{id}")]
        public async Task<IActionResult> GetBranchbyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var branchDetails = _context.BranchDetails.Where(m => m.CompanyId == id);
            if (branchDetails == null)
            {
                return NotFound();
            }

            return Ok(branchDetails);
        }

        // GET: api/BranchDetails/Company/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetBranchbyId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var branchDetails = _context.BranchDetails.Where(m => m.BranchId == id);
            if (branchDetails == null)
            {
                return NotFound();
            }

            return Ok(branchDetails);
        }

        // PUT: api/BranchDetails/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBranchDetails([FromRoute] int id, [FromBody] BranchDetails branchDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != branchDetails.BranchId)
            {
                return BadRequest();
            }

            _context.Entry(branchDetails).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BranchDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPut("Disable/{id}")]
        public async Task<IActionResult> PutStatusBranch([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            BranchDetails branch = await _context.BranchDetails.SingleOrDefaultAsync(m => m.BranchId == id);

            if (branch == null) return BadRequest();
            branch.Status = "Disabled";
            _context.Entry(branch).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok("User Has been Deactivated");
        }

        // POST: api/BranchDetails
        [HttpPost]
        public async Task<IActionResult> PostBranchDetails([FromBody] BranchDetails branchDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            branchDetails.BranchCode = await seq.GetCode("Branch") + branchDetails.CompanyId;
            branchDetails.CreatedDate = DateTime.UtcNow;
            _context.BranchDetails.Add(branchDetails);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBranchDetails", new { id = branchDetails.BranchId }, branchDetails);
        }

        // DELETE: api/BranchDetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBranchDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var branchDetails = await _context.BranchDetails.SingleOrDefaultAsync(m => m.BranchId == id);
            if (branchDetails == null)
            {
                return NotFound();
            }

            _context.BranchDetails.Remove(branchDetails);
            await _context.SaveChangesAsync();

            return Ok(branchDetails);
        }

        private bool BranchDetailsExists(int id)
        {
            return _context.BranchDetails.Any(e => e.BranchId == id);
        }
    }
}