﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Upload")]
    public class UploadController : Controller
    {
        private readonly CmsDbContext _context;

        public UploadController(CmsDbContext context)
        {
            _context = context;
        }

        [HttpPost("{filetype}")]
        public async Task<IActionResult> UploadFile(string filetype, IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("File not selected");
            if (string.IsNullOrEmpty(filetype))
                return Content("File type required");
            string[] filedetails = file.FileName.Split('.');
            filedetails[0] = DateTime.Now.ToString(@"yyyy-MM-dd ") + "T" + DateTime.Now.ToString(@" hh mm ss");
            string filename = string.Concat(filedetails[0], ".", filedetails[1]);
            var path = Path.Combine(
                Directory.GetCurrentDirectory(), $"Files/{filetype}",
                filename);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return Ok($"{filename}");
        }
    }
}