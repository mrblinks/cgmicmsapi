﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;
using CmsApi.MyServices;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Transactions")]
    public class TransactionsController : Controller
    {
        private readonly CmsDbContext _context;
        SequenceCode seq;

        public TransactionsController(CmsDbContext context)
        {
            _context = context;
            seq = new SequenceCode(context);
        }

        // GET: api/Transactions
        [HttpGet]
        public IEnumerable<Transaction> GetTransactions()
        {
            return _context.Transactions
                .Include(a => a.PaymentMethod).Include(c => c.Currency);
        }

        // GET: api/Transactions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTransaction([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var transaction = _context.Transactions.Where(m => m.TransactionId == id)
                .Include(a => a.PaymentMethod).Include(c => c.Currency).Include(c => c.GetGeneralLedgerCode);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        [HttpGet("Members")]
        public async Task<IActionResult> GetMembersTransaction()
        {
            var transaction = _context.MenTransactions.Include(m => m.Member).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        [HttpGet("Accounts")]
        public async Task<IActionResult> GetAccountsTransaction()
        {
            var transaction = _context.AcctTransactions.Include(m => m.GeneralLedgerCode).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        [HttpGet("Members/Church/{id}")]
        public async Task<IActionResult> GetChurchMembersTransaction([FromRoute] int id)
        {
            var transaction = _context.MenTransactions.Where(a=>a.CompanyId == id)
                .Include(m => m.Member).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        [HttpGet("Members/Branch/{id}")]
        public async Task<IActionResult> GetBranchMembersTransaction([FromRoute] int id)
        {
            var transaction = _context.MenTransactions.Where(a => a.BranchId == id)
                .Include(m => m.Member).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        [HttpGet("Receive/Church/{id}")]
        public async Task<IActionResult> GetChurchReceiveTransaction([FromRoute] int id)
        {
            var transaction = _context.ReceiptTransactions.Where(a => a.CompanyId == id)
                .Include(m => m.GeneralLedgerCode).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        [HttpGet("Receive/Branch/{id}")]
        public async Task<IActionResult> GetBranchReceiveTransaction([FromRoute] int id)
        {
            var transaction = _context.ReceiptTransactions.Where(a => a.BranchId == id)
                .Include(m => m.GeneralLedgerCode).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }
        
        [HttpGet("Accounts/Church/{id}")]
        public async Task<IActionResult> GetChurchAccountsTransaction([FromRoute] int id)
        {
            var transaction = _context.AcctTransactions.Where(a => a.CompanyId == id)
                .Include(m => m.GeneralLedgerCode).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }
        
        [HttpGet("Accounts/Branch/{id}")]
        public async Task<IActionResult> GetBranchAccountTransaction([FromRoute] int id)
        {
            var transaction = _context.AcctTransactions.Where(a => a.BranchId == id)
                .Include(m => m.GeneralLedgerCode).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        [HttpGet("Member/{id}")]
        public async Task<IActionResult> GetChurchSingleMemberTransaction([FromRoute] int id)
        {
            var transaction = _context.MenTransactions.Where(a => a.MemberId == id)
                .Include(m => m.Member).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }
        
        [HttpGet("Account/{id}")]
        public async Task<IActionResult> GetChurchSingleAccountTransaction([FromRoute] int id)
        {
            var transaction = _context.AcctTransactions.Where(a => a.GeneralLedgerCodeId == id)
                .Include(m => m.GeneralLedgerCode).Include(t => t.Transaction)
                .Include(a => a.Transaction.PaymentMethod).Include(c => c.Transaction.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        // GET: api/Transactions/GenerelLedgerCode/5
        [HttpGet("GeneralLedgerCode/{id}")]
        public async Task<IActionResult> GetChurchTypeTransaction([FromRoute] int id)
        {
            var transaction = _context.Transactions.Where(a => a.GeneralLedgerCodeId == id)
                .Include(a => a.PaymentMethod).Include(c => c.Currency);

            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        // GET: api/Transactions/Company/5
        [HttpGet("Church/{id}")]
        public async Task<IActionResult> GetChurchTransaction([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var transaction = _context.Transactions.Where(m => m.CompanyId == id)
                .Include(a => a.PaymentMethod).Include(c => c.Currency); ;
            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        // GET: api/Transactions/Branch/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetBranchTransaction([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var transactions = _context.Transactions.Where(m => m.BranchId == id)
                .Include(a => a.PaymentMethod).Include(c => c.Currency);
            if (transactions == null)
            {
                return NotFound();
            }
            return Ok(transactions);
        }

        // POST: api/Transactions
        [HttpPut("Undo/{id}")]
        public async Task<IActionResult> UndoTransaction([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            ReceiptTransactions receipt = _context.ReceiptTransactions.Where(a=>a.ReceiptTransactionId == id).FirstOrDefault();
            if (receipt == null) return NotFound($"Receipt Transaction with id {id} does not exist");
            Transaction transact = await _context.Transactions.FindAsync(receipt.TransactionId);
            GeneralLedgerCode glcode = await _context.GeneralLedgerCodes.FindAsync(transact.GeneralLedgerCodeId);
            
            try
            {
                Transaction transaction = new Transaction();
                if (transact.TransType.ToLower().Contains("debit"))
                {
                    glcode.Balance -= transact.Amount;
                    if (glcode.Balance < 0) return BadRequest($"Not Enough Fund to RollBack Transaction");
                    transaction.TransType = "CREDIT";
                }
                else if (transact.TransType.ToLower().Contains("credit"))
                {
                    glcode.Balance += transact.Amount;
                    transaction.TransType = "DEBIT";
                }
                
                ReceiptTransactions memTransactions = new ReceiptTransactions();
                string transCode = await seq.GetCode("Transaction");
                transaction.TransCode = transCode;
                transaction.Reference = "UNDO " + transact.Reference;
                transaction.PaymentMethodId = transact.PaymentMethodId;
                transaction.CurrencyId = transact.CurrencyId;
                transaction.Amount = transact.Amount;
                transaction.BranchId = transact.BranchId;
                transaction.CompanyId = transact.CompanyId;
                transaction.CreatedUserId = transact.CreatedUserId;
                transaction.TransSource = "UNDO " + transact.TransSource;
                transaction.CreatedDate = DateTime.Now;
                _context.Entry(glcode).State = EntityState.Modified;
                _context.Transactions.Add(transaction);
                await _context.SaveChangesAsync();

                memTransactions.GeneralLedgerCodeId = transaction.GeneralLedgerCodeId;
                memTransactions.TransactionId = transaction.TransactionId;
                memTransactions.BranchId = transaction.BranchId;
                memTransactions.CompanyId = transaction.CompanyId;
                memTransactions.CreatedUserId = transaction.CreatedUserId;
                memTransactions.CreatedDate = DateTime.UtcNow;
                _context.ReceiptTransactions.Add(memTransactions);
                await _context.SaveChangesAsync();

                return Ok(new { Status = "OK", Message = "Payment Successfully", Transaction = transaction, Output = "Member Payment Has been Successfully Entered" });
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }

        // POST: api/Transactions
        [HttpPut("Member/Undo/{id}")]
        public async Task<IActionResult> UndoMemberTransaction([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            MemTransactions member = _context.MenTransactions.Where(a => a.MemTransactionsId == id).FirstOrDefault();
            if (member == null) return NotFound($"Member Transaction with id {id} does not exist");
            Transaction transact = await _context.Transactions.FindAsync(member.TransactionId);
            GeneralLedgerCode glcode = await _context.GeneralLedgerCodes.FindAsync(transact.GeneralLedgerCodeId);

            try
            {
                Transaction transaction = new Transaction();
                if (transact.TransType.ToLower().Contains("debit"))
                {
                    glcode.Balance -= transact.Amount;
                    if (glcode.Balance < 0) return BadRequest($"Not Enough Fund to RollBack Transaction");
                    transaction.TransType = "CREDIT";
                }
                else if (transact.TransType.ToLower().Contains("credit"))
                {
                    glcode.Balance += transact.Amount;
                    transaction.TransType = "DEBIT";
                }

                MemTransactions memTransactions = new MemTransactions();
                string transCode = await seq.GetCode("Transaction");
                transaction.TransCode = transCode;
                transaction.Reference = "UNDO " + transact.Reference;
                transaction.PaymentMethodId = transact.PaymentMethodId;
                transaction.GeneralLedgerCodeId = transact.GeneralLedgerCodeId;
                transaction.CurrencyId = transact.CurrencyId;
                transaction.Amount = transact.Amount;
                transaction.BranchId = transact.BranchId;
                transaction.CompanyId = transact.CompanyId;
                transaction.CreatedUserId = transact.CreatedUserId;
                transaction.TransSource = "UNDO " + transact.TransSource;
                transaction.CreatedDate = DateTime.Now;
                _context.Entry(glcode).State = EntityState.Modified;
                _context.Transactions.Add(transaction);
                await _context.SaveChangesAsync();

                memTransactions.MemberId = member.MemberId;
                memTransactions.TransactionId = transaction.TransactionId;
                memTransactions.BranchId = transaction.BranchId;
                memTransactions.CompanyId = transaction.CompanyId;
                memTransactions.CreatedUserId = transaction.CreatedUserId;
                memTransactions.CreatedDate = DateTime.UtcNow;
                _context.MenTransactions.Add(memTransactions);
                await _context.SaveChangesAsync();

                return Ok(new { Status = "OK", Message = "Successfull", Output = "Member Payment has been Undo Successfully" });
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }
        }

        [HttpPut("Payment/Undo/{id}")]
        public async Task<IActionResult> UndoTransferAmountTransaction([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            AcctTransactions Account = await _context.AcctTransactions.FindAsync(id);
            if (Account == null) return NotFound($"Transfer Transaction with id {id} does not exist");

            Transaction transaction = await _context.Transactions.FindAsync(Account.TransactionId);
            GeneralLedgerCode glcode = await _context.GeneralLedgerCodes.FindAsync(Account.GlCodeId);
            GeneralLedgerCode account = await _context.GeneralLedgerCodes.FindAsync(Account.GeneralLedgerCodeId);
            
            if (glcode == null) return NotFound($"Payment Type with id {Account.GlCodeId} does not exist");
            if (account == null) return NotFound($"Account with id {Account.GeneralLedgerCodeId} does not exist");

            try
            {
                AcctTransactions acctTransaction = new AcctTransactions();
                Transaction Transact = new Transaction();
                Transaction acctTransact = new Transaction();
                if (transaction.TransType.ToLower().Contains("debit"))
                {
                    account.Balance -= transaction.Amount;
                    if (account.Balance < 0) return BadRequest($"Not Enough Fund to Perform Transaction");
                    glcode.Balance += transaction.Amount;
                    Transact.TransType = "DEBIT";
                    acctTransact.TransType = "CREDIT";
                }
                else if(transaction.TransType.ToLower().Contains("credit"))
                {
                    glcode.Balance -= transaction.Amount;
                    if (glcode.Balance < 0) return BadRequest($"Not Enough Fund to RollBack Transaction");
                    account.Balance += transaction.Amount;
                    Transact.TransType = "CREDIT";
                    acctTransact.TransType = "DEBIT";
                }

                string transCode = await seq.GetCode("Transaction");
                Transact.TransCode = transCode;
                Transact.TransSource = "UNDO " + transaction.TransSource;
                Transact.GeneralLedgerCodeId = Account.GlCodeId;
                Transact.CreatedDate = DateTime.Now;
                Transact.Reference = "UNDO " + transaction.Reference;
                Transact.PaymentMethodId = transaction.PaymentMethodId;
                Transact.CurrencyId = transaction.CurrencyId;
                Transact.Amount = transaction.Amount;
                Transact.BranchId = transaction.BranchId;
                Transact.CompanyId = transaction.CompanyId;
                Transact.CreatedUserId = transaction.CreatedUserId;
                Transact.CreatedDate = DateTime.Now;
                _context.Entry(glcode).State = EntityState.Modified;
                _context.Entry(account).State = EntityState.Modified;
                _context.Transactions.Add(Transact);

                acctTransact.TransSource = "UNDO " + transaction.TransSource;
                acctTransact.GeneralLedgerCodeId = Account.GeneralLedgerCodeId;
                acctTransact.CreatedDate = DateTime.Now;
                acctTransact.Reference = "UNDO " + transaction.Reference;
                acctTransact.TransCode = transCode;
                acctTransact.PaymentMethodId = transaction.PaymentMethodId;
                acctTransact.CurrencyId = transaction.CurrencyId;
                acctTransact.Amount = transaction.Amount;
                acctTransact.BranchId = transaction.BranchId;
                acctTransact.CompanyId = transaction.CompanyId;
                acctTransact.CreatedUserId = transaction.CreatedUserId;
                _context.Transactions.Add(acctTransact);
                await _context.SaveChangesAsync();
                
                acctTransaction.GeneralLedgerCodeId = Account.GeneralLedgerCodeId;
                acctTransaction.GlCodeId = Account.GlCodeId;
                acctTransaction.TransactionId = acctTransact.TransactionId;
                acctTransaction.BranchId = transaction.BranchId;
                acctTransaction.CompanyId = transaction.CompanyId;
                acctTransaction.CreatedUserId = transaction.CreatedUserId;
                acctTransaction.TransactionId = acctTransact.TransactionId;
                acctTransaction.CreatedDate = DateTime.UtcNow;
                _context.AcctTransactions.Add(acctTransaction);
                await _context.SaveChangesAsync();

              return Ok(new { Status = "OK", Message = "Transaction Revert Successfully", Output = "Transaction has been Successfully Reverted" });
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }
        
        // POST: api/Transactions
        [HttpPost]
        public async Task<IActionResult> PostTransaction([FromBody] Transaction transaction)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            GeneralLedgerCode glcode = await _context.GeneralLedgerCodes.FindAsync(transaction.GeneralLedgerCodeId);
            PaymentMethod paymentMethod = await _context.PaymentMethod.FindAsync(transaction.PaymentMethodId);
            Currency currency = await _context.PaymentType.FindAsync(transaction.CurrencyId);

            if (glcode == null) return NotFound($"Payment Type with id {transaction.GeneralLedgerCodeId} does not exist");
            if (paymentMethod == null) return NotFound($"PaymentMethod with id {transaction.PaymentMethodId} does not exist");
            if (currency == null) return NotFound($"Currency with id {transaction.CurrencyId} does not exist");

            try
            {
                ReceiptTransactions memTransactions = new ReceiptTransactions();
                string transCode = await seq.GetCode("Transaction");
                
                glcode.Balance += transaction.Amount;
                transaction.TransCode = transCode;
                transaction.TransType = "DEBIT";
                _context.Entry(glcode).State = EntityState.Modified;
                _context.Transactions.Add(transaction);
                await _context.SaveChangesAsync();

                memTransactions.GeneralLedgerCodeId = transaction.GeneralLedgerCodeId;
                memTransactions.TransactionId = transaction.TransactionId;
                memTransactions.BranchId = transaction.BranchId;
                memTransactions.CompanyId = transaction.CompanyId;
                memTransactions.CreatedUserId = transaction.CreatedUserId;
                memTransactions.CreatedDate = DateTime.UtcNow;
                _context.ReceiptTransactions.Add(memTransactions);
                await _context.SaveChangesAsync();

                return Ok(new { Status = "OK", Message = "Payment Successfully", Transaction = transaction, Output = "Member Payment Has been Successfully Entered" });
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }

        // POST: api/Transactions/Members
        [HttpPost("Members")]
        public async Task<IActionResult> PostMembersTransaction([FromBody] Transaction transaction)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            GeneralLedgerCode glcode = await _context.GeneralLedgerCodes.FindAsync(transaction.GeneralLedgerCodeId);
            Member member = await _context.Members.FindAsync(transaction.MemberId);
            PaymentMethod paymentMethod = await _context.PaymentMethod.FindAsync(transaction.PaymentMethodId);
            Currency currency = await _context.PaymentType.FindAsync(transaction.CurrencyId);

            if (glcode == null) return NotFound($"Payment Type with id {transaction.GeneralLedgerCodeId} does not exist");
            if (member == null) return NotFound($"Member with id {transaction.MemberId} does not exist");
            if (paymentMethod == null) return NotFound($"PaymentMethod with id {transaction.PaymentMethodId} does not exist");
            if (currency == null) return NotFound($"Currency with id {transaction.CurrencyId} does not exist");

            try
            {
                string transCode = await seq.GetCode("Transaction");
                MemTransactions memTransactions = new MemTransactions();

                glcode.Balance += transaction.Amount;
                transaction.TransCode = transCode;
                transaction.TransType = "DEBIT";
                _context.Entry(glcode).State = EntityState.Modified;
                _context.Transactions.Add(transaction);
                await _context.SaveChangesAsync();

                memTransactions.MemberId = transaction.MemberId;
                memTransactions.TransactionId = transaction.TransactionId;
                memTransactions.BranchId = transaction.BranchId;
                memTransactions.CompanyId = transaction.CompanyId;
                memTransactions.CreatedUserId = transaction.CreatedUserId;
                memTransactions.CreatedDate = DateTime.UtcNow;
                _context.MenTransactions.Add(memTransactions);

                await _context.SaveChangesAsync();

                return Ok(new { Status = "OK", Message = "Payment Successfully", Transaction = transaction, Output = "Member Payment Has been Successfully Entered" });
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }
        }

        [HttpPost("Transfer")]
        public async Task<IActionResult> PostTransferAmountTransaction([FromBody] Transaction transaction)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            GeneralLedgerCode glcode = await _context.GeneralLedgerCodes.FindAsync(transaction.GeneralLedgerCodeId);
            GeneralLedgerCode account = await _context.GeneralLedgerCodes.FindAsync(transaction.AccountId);
            PaymentMethod paymentMethod = await _context.PaymentMethod.FindAsync(transaction.PaymentMethodId);
            Currency currency = await _context.PaymentType.FindAsync(transaction.CurrencyId);

            if (glcode == null) return NotFound($"Payment Type with id {transaction.GeneralLedgerCodeId} does not exist");
            if (account == null) return NotFound($"Account with id {transaction.AccountId} does not exist");
            if (paymentMethod == null) return NotFound($"PaymentMethod with id {transaction.PaymentMethodId} does not exist");
            if (currency == null) return NotFound($"Currency with id {transaction.Currency} does not exist");

            try
            {
                string transCode = await seq.GetCode("Transaction");
                AcctTransactions acctTransaction = new AcctTransactions();
                Transaction acctTransact = new Transaction();

                glcode.Balance -= transaction.Amount;
                if (glcode.Balance < 0) return BadRequest($"Not Enough Fund to Perform Transaction");
                
                account.Balance += transaction.Amount;
                transaction.TransCode = transCode;
                transaction.TransType = "CREDIT";
                transaction.CreatedDate = DateTime.Now;
                _context.Entry(glcode).State = EntityState.Modified;
                _context.Entry(account).State = EntityState.Modified;
                _context.Transactions.Add(transaction);
                //await _context.SaveChangesAsync();

                acctTransact.TransType = "DEBIT";
                acctTransact.CreatedDate = DateTime.Now;
                acctTransact.GeneralLedgerCodeId = transaction.AccountId;
                acctTransact.Reference = transaction.Reference;
                acctTransact.TransCode = transaction.TransCode;
                acctTransact.PaymentMethodId = transaction.PaymentMethodId;
                acctTransact.CurrencyId = transaction.CurrencyId;
                acctTransact.TransSource = transaction.TransSource;
                acctTransact.Amount = transaction.Amount;
                acctTransact.BranchId = transaction.BranchId;
                acctTransact.CompanyId = transaction.CompanyId;
                acctTransact.CreatedUserId = transaction.CreatedUserId;
                _context.Transactions.Add(acctTransact);
                await _context.SaveChangesAsync();

                acctTransaction.GeneralLedgerCodeId = transaction.AccountId;
                acctTransaction.GlCodeId = transaction.GeneralLedgerCodeId;
                acctTransaction.TransactionId = acctTransact.TransactionId;
                acctTransaction.BranchId = transaction.BranchId;
                acctTransaction.CompanyId = transaction.CompanyId;
                acctTransaction.CreatedUserId = transaction.CreatedUserId;
                acctTransaction.CreatedDate = DateTime.UtcNow;
                _context.AcctTransactions.Add(acctTransaction);

                await _context.SaveChangesAsync();

                return Ok(new { Status = "OK", Message = "Transfer Successfully", Transaction = transaction, Output = "Amount has been Successfully Transfered" });
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }

        [HttpPost("Payment")]
        public async Task<IActionResult> PostAccountsTransaction([FromBody] Transaction transaction)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            GeneralLedgerCode glcode = await _context.GeneralLedgerCodes.FindAsync(transaction.GeneralLedgerCodeId);
            if (glcode == null) return NotFound($"Payment Type with id {transaction.GeneralLedgerCodeId} does not exist");
            GeneralLedgerCode account = await _context.GeneralLedgerCodes.FindAsync(transaction.AccountId);
            if (account == null) return NotFound($"Account with id {transaction.AccountId} does not exist");
            PaymentMethod paymentMethod = await _context.PaymentMethod.FindAsync(transaction.PaymentMethodId);
            if (paymentMethod == null) return NotFound($"PaymentMethod with id {transaction.PaymentMethodId} does not exist");
            Currency currency = await _context.PaymentType.FindAsync(transaction.CurrencyId);
            if (currency == null) return NotFound($"Currency with id {transaction.Currency} does not exist");

            try
            {
                account.Balance -= transaction.Amount;
                if (account.Balance < 0) return BadRequest($"Not Enough Fund in Account to Perform Transaction");

                string transCode = await seq.GetCode("Transaction");
                AcctTransactions acctTransaction = new AcctTransactions();
                Transaction acctTransact = new Transaction();

                glcode.Balance += transaction.Amount;
                transaction.TransCode = transCode;
                transaction.TransType = "DEBIT";
                _context.Entry(glcode).State = EntityState.Modified;
                _context.Entry(account).State = EntityState.Modified;
                _context.Transactions.Add(transaction);

                acctTransact.TransType = "CREDIT";
                acctTransact.GeneralLedgerCodeId = transaction.AccountId;
                acctTransact.CreatedDate = DateTime.Now;
                acctTransact.Reference = transaction.Reference;
                acctTransact.TransCode = transaction.TransCode;
                acctTransact.PaymentMethodId = transaction.PaymentMethodId;
                acctTransact.CurrencyId = transaction.CurrencyId;
                acctTransact.TransSource = transaction.TransSource;
                acctTransact.Amount = transaction.Amount;
                acctTransact.BranchId = transaction.BranchId;
                acctTransact.CompanyId = transaction.CompanyId;
                acctTransact.CreatedUserId = transaction.CreatedUserId;
                _context.Transactions.Add(acctTransact);
                await _context.SaveChangesAsync();

                acctTransaction.GeneralLedgerCodeId = transaction.AccountId;
                acctTransaction.GlCodeId = transaction.GeneralLedgerCodeId;
                acctTransaction.TransactionId = acctTransact.TransactionId;
                acctTransaction.BranchId = transaction.BranchId;
                acctTransaction.CompanyId = transaction.CompanyId;
                acctTransaction.CreatedUserId = transaction.CreatedUserId;
                acctTransaction.CreatedDate = DateTime.UtcNow;
                _context.AcctTransactions.Add(acctTransaction);
                await _context.SaveChangesAsync();

             return Ok(new { Status = "OK", Message = "Payment Successfull", Transaction = transaction, Output = "Payment has been made Successfully" });
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }

        // DELETE: api/Transactions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTransaction([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var transaction = await _context.Transactions.SingleOrDefaultAsync(m => m.TransactionId == id);
            if (transaction == null)
            {
                return NotFound();
            }

            _context.Transactions.Remove(transaction);
            await _context.SaveChangesAsync();

            return Ok(transaction);
        }
        
        private bool TransactionExists(int id)
        {
            return _context.Transactions.Any(e => e.TransactionId == id);
        }
    }
}