﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Departments")]
    public class DepartmentsController : Controller
    {
        private readonly CmsDbContext _context;

        public DepartmentsController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/Departments
        [HttpGet]
        public IEnumerable<Department> GetDepartment()
        {
            return _context.Department.Include(a => a.Branch).Include(a => a.CompanyDetails);
        }

        // GET: api/Departments/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDepartment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var department = await _context.Department.SingleOrDefaultAsync(m => m.DepartmentId == id);

            if (department == null)
            {
                return NotFound();
            }

            return Ok(department);
        }

        // GET: api/Transactions/Company/5
        [HttpGet("Church/{id}")]
        public async Task<IActionResult> GetChurchDepartment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var department = _context.Department.Where(m => m.CompanyId == id);
            if (department == null)
            {
                return NotFound();
            }

            return Ok(department);
        }

        // GET: api/Department/Branch/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetBranchDepartment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var department = _context.Department.Where(m => m.BranchId == id);
            if (department == null)
            {
                return NotFound();
            }
            return Ok(department);
        }

        // PUT: api/Departments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDepartment([FromRoute] int id, [FromBody] Department department)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != department.DepartmentId)
            {
                return BadRequest();
            }
            department.CreatedDate = DateTime.Now;
            _context.Entry(department).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DepartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Departments
        [HttpPost]
        public async Task<IActionResult> PostDepartment([FromBody] Department department)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Department.Add(department);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDepartment", new { id = department.DepartmentId }, department);
        }

        [HttpPost("Batch")]
        public async Task<IActionResult> PostBatchDpt([FromBody] Data data)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            foreach (var a in data.data)
            {
                a.BranchId = data.BranchId;
                a.CompanyId = data.CompanyId;
                a.CreatedUserId = data.CreatedUserId;
                a.CreatedDate = DateTime.UtcNow;
                _context.Department.Add(a);
                //await seq.Sms(a);
            }

            await _context.SaveChangesAsync();
        return Ok(new { Status = "OK", Message = "Successfully Added", Output = "Departments has been added" });
        }
        
        // DELETE: api/Departments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDepartment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var department = await _context.Department.SingleOrDefaultAsync(m => m.DepartmentId == id);
            if (department == null)
            {
                return NotFound();
            }

            _context.Department.Remove(department);
            await _context.SaveChangesAsync();

            return Ok(department);
        }

        private bool DepartmentExists(int id)
        {
            return _context.Department.Any(e => e.DepartmentId == id);
        }

        public class Data : BaseModel
        {
            public List<Department> data { get; set; }
        }
    }
}