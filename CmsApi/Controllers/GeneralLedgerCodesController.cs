﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/GeneralLedgerCodes")]
    public class GeneralLedgerCodesController : Controller
    {
        private readonly CmsDbContext _context;

        public GeneralLedgerCodesController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/GeneralLedgerCodes
        [HttpGet]
        public IEnumerable<GeneralLedgerCode> GetGeneralLedgerCodes()
        {
            return _context.GeneralLedgerCodes.Include(a=>a.Branch).Include(a=>a.CompanyDetails);
        }

        // GET: api/GeneralLedgerCodes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGeneralLedgerCode([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var generalLedgerCode = await _context.GeneralLedgerCodes.SingleOrDefaultAsync(m => m.GeneralLedgerCodeId == id);

            if (generalLedgerCode == null)
            {
                return NotFound();
            }
            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Company/5
        [HttpGet("Church/{id}")]
        public async Task<IActionResult> GetGlcodebyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.CompanyId == id)
                .Include(c => c.Currency);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }

            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Branch/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetGlcodebyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.BranchId == id)
                .Include(c=>c.Currency);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }
            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Credit/
        [HttpGet("Credit")]
        public async Task<IActionResult> GetCreditGlcode()
        {
            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.BalanceType.ToLower().Contains("credit") 
                            && m.GlType.ToLower() != "cashbook").Include(c => c.Currency)
                            .Include(a => a.Branch).Include(a => a.CompanyDetails);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }

            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Credit/Company/5
        [HttpGet("Credit/Church/{id}")]
        public async Task<IActionResult> GetCreditGlcodebyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.CompanyId == id && m.BalanceType
                            .ToLower().Contains("credit") && m.GlType.ToLower() != "cashbook")
                .Include(c => c.Currency);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }

            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Credit/Branch/5
        [HttpGet("Credit/Branch/{id}")]
        public async Task<IActionResult> GetCreditGlcodebyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.BranchId == id && 
                    m.BalanceType.ToLower().Contains("credit") && m.GlType.ToLower() != "cashbook")
                    .Include(c => c.Currency);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }
            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Credit/Company/5
        [HttpGet("CashBook")]
        public async Task<IActionResult> GetCashbookGlcode()
        {
            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.GlType.ToLower().Contains("cashbook"))
                .Include(c => c.Currency).Include(a => a.Branch).Include(a => a.CompanyDetails);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }

            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Credit/Company/5
        [HttpGet("CashBook/Church/{id}")]
        public async Task<IActionResult> GetCashbookGlcodebyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.CompanyId == id && m.GlType.ToLower().Contains("cashbook"))
                .Include(c => c.Currency);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }

            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Credit/Branch/5
        [HttpGet("CashBook/Branch/{id}")]
        public async Task<IActionResult> GetCashbookGlcodebyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.BranchId == id && m.GlType.ToLower()
                            .Contains("cashbook")).Include(c => c.Currency); ;
            if (generalLedgerCode == null)
            {
                return NotFound();
            }
            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Debit
        [HttpGet("Debit")]
        public async Task<IActionResult> GetDebitGlcode()
        {
            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.BalanceType.ToLower()
                            .Contains("debit")).Include(c => c.Currency)
                            .Include(a => a.Branch).Include(a => a.CompanyDetails);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }
            return Ok(generalLedgerCode);
        }

        // GET: api/GeneralLedgerCodes/Debit/Company/5
        [HttpGet("Debit/Church/{id}")]
        public async Task<IActionResult> GetDebitGlcodebyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.CompanyId == id && m.BalanceType.ToLower().Contains("debit"))
                .Include(c => c.Currency);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }

            return Ok(generalLedgerCode);
        }
        
        // GET: api/GeneralLedgerCodes/Debit/Branch/5
        [HttpGet("Debit/Branch/{id}")]
        public async Task<IActionResult> GetDebitGlcodebyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var generalLedgerCode = _context.GeneralLedgerCodes.Where(m => m.BranchId == id && m.BalanceType.ToLower()
                            .Contains("debit")).Include(c => c.Currency)
                            .Include(a => a.Branch).Include(a => a.CompanyDetails);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }
            return Ok(generalLedgerCode);
        }

        // PUT: api/GeneralLedgerCodes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGeneralLedgerCode([FromRoute] int id, [FromBody] GeneralLedgerCode generalLedgerCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != generalLedgerCode.GeneralLedgerCodeId)
            {
                return BadRequest();
            }

            _context.Entry(generalLedgerCode).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GeneralLedgerCodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GeneralLedgerCodes
        [HttpPost]
        public async Task<IActionResult> PostGeneralLedgerCode([FromBody] GeneralLedgerCode generalLedgerCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.GeneralLedgerCodes.Add(generalLedgerCode);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGeneralLedgerCode", new { id = generalLedgerCode.GeneralLedgerCodeId }, generalLedgerCode);
        }

        // DELETE: api/GeneralLedgerCodes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGeneralLedgerCode([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var generalLedgerCode = await _context.GeneralLedgerCodes.SingleOrDefaultAsync(m => m.GeneralLedgerCodeId == id);
            if (generalLedgerCode == null)
            {
                return NotFound();
            }

            _context.GeneralLedgerCodes.Remove(generalLedgerCode);
            await _context.SaveChangesAsync();

            return Ok(generalLedgerCode);
        }

        private bool GeneralLedgerCodeExists(int id)
        {
            return _context.GeneralLedgerCodes.Any(e => e.GeneralLedgerCodeId == id);
        }
    }
}