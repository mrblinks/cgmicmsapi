﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using CmsApi.Models.Identity;
using Microsoft.Extensions.Configuration;
using CmsApi.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.DataProtection;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using CmsApi.Extensions;
using CmsApi.MyServices;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    public class LoginController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly CmsDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private readonly IDataProtector _protector;

        public LoginController(UserManager<ApplicationUser> usrMgr, SignInManager<ApplicationUser> signinMgr,
            IConfiguration configuration, CmsDbContext context, ILogger<LoginController> logger,IEmailSender emailSender, IDataProtectionProvider provider)
        {
            _userManager = usrMgr;
            _signInManager = signinMgr;
            _configuration = configuration;
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            _protector = provider.CreateProtector("Contoso.MyClass.v1");
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Logins([FromBody] Login model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user == null)
            {
                user = await _userManager.FindByEmailAsync(model.Username);
                if (user == null) return BadRequest("Invalid Username: user doesn't Exist");
            }

            Users users = _context.Users.Where(a => a.Username == model.Username || a.Email == model.Username)
                .Include(b=>b.Branch).FirstOrDefault();
            if (users.Status.ToLower() == "disabled") return BadRequest("You have Been Disabled by Administrator please Contact Church Admin");
            if (users.Branch.Status.ToLower() == "disabled") return BadRequest("Your Branch have Been Disabled by Administrator please Contact Acyst Tech");
            if (users.Branch.ExpiryDate < DateTime.Now) return BadRequest("Your Church License has Expired please Contact Acyst Tech for Renewal");

            var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, true);
            if (!result.Succeeded) return BadRequest($"Incorrect Password for {model.Username}");

            var userClaims = await _userManager.GetClaimsAsync(user);

            userClaims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.UserName));
            userClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("qwertyuiopasdfghjklzxcvbnm123456"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: "http://localhost:50563",
                audience: "http://localhost:50563",
                claims: userClaims,
                expires: DateTime.Now.AddHours(12),
                signingCredentials: creds);
            var addresult = Config.AddConcurrentUsers(user.UserName);
            if (!addresult)
            {
                Config.RemoveConcurrentUsers(user.UserName);
                Config.AddConcurrentUsers(user.UserName);
            }
            users.LastLogin = DateTime.UtcNow;
            users.Status = "Online";
            _context.Entry(users).State = EntityState.Modified;
            _context.SaveChanges();

            return Ok(new { access_token = new JwtSecurityTokenHandler().WriteToken(token), expires_in_hours = 12, date = DateTime.Now, User = users });
        }

        [HttpPost("AdminLogin")]
        public async Task<IActionResult> AdminLogins([FromBody] Login model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user == null)
            {
                user = await _userManager.FindByEmailAsync(model.Username);
                if (user == null) return BadRequest("Invalid Username: user doesn't Exist");
            }
            Users users = _context.Users.Where(a => a.Username == model.Username || a.Email == model.Username).FirstOrDefault();
            if (users.Status.ToLower() == "disabled") return BadRequest("You have Been Disabled by Administrator");
            if (users.UserType.ToLower() != "admin") return BadRequest("You Are Not An Administrator");

            var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, true);
            if (!result.Succeeded) return BadRequest($"Incorrect Password for {model.Username}");
            
            var userClaims = await _userManager.GetClaimsAsync(user);

            userClaims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.UserName));
            userClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("qwertyuiopasdfghjklzxcvbnm123456"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: "http://localhost:50563",
                audience: "http://localhost:50563",
                claims: userClaims,
                expires: DateTime.Now.AddHours(12),
                signingCredentials: creds);
            var addresult = Config.AddConcurrentUsers(user.UserName);
            if (!addresult)
            {
                Config.RemoveConcurrentUsers(user.UserName);
                Config.AddConcurrentUsers(user.UserName);
            }
            users.LastLogin = DateTime.UtcNow;
            users.Status = "Online";
            _context.Entry(users).State = EntityState.Modified;
            _context.SaveChanges();

            return Ok(new { access_token = new JwtSecurityTokenHandler().WriteToken(token), expires_in_hours = 12, date = DateTime.Now, User = _context.Users.FirstOrDefault(a => a.Username == model.Username || a.Email == model.Username) });
        }
        
        [HttpPost("Logout/{username}")]
        public async Task<IActionResult> Logout(string username)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = _context.Users.FirstOrDefault(a => a.Username == username);
            user.LastLogOut = DateTime.UtcNow;
            user.Status = "Active";

            _context.Entry(user).State = EntityState.Modified;
            var loggedout = Config.RemoveConcurrentUsers(username);
            if (loggedout)
            {
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User {username} logged out");
                return Ok("Logout successfull");
            }
            return BadRequest("Logout not successfull");
        }

        [HttpPost("Changepassword")]
        public async Task<IActionResult> PasswordChange([FromBody] ChangePassword model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var appuser = await _userManager.FindByNameAsync(model.Username);
            var user = _context.Users.FirstOrDefault(a => a.Username == model.Username);
            if (appuser == null || user == null) return NotFound("User does not exist");
            var resul = await _signInManager.CheckPasswordSignInAsync(appuser, model.OldPassword, true);
            if (!resul.Succeeded) return BadRequest("Current Password is incorrect");
            //if (model.OldPassword != user.Password) return BadRequest("Current Password is incorrect.");

            IdentityResult result = await _userManager.ChangePasswordAsync(appuser, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                //user.Password = model.NewPassword;
                //_context.Entry(user).State = EntityState.Modified;
                _context.SaveChanges();
                return Ok("Password Changed Successfully");
            }
            else
            {
                return Content("Could not change Password");
            }
        }
        
        [HttpGet("Resetuser/{id}")]
        public async Task<IActionResult> PasswordReset([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = _context.Users.FirstOrDefault(a => a.UserId == id);
            var appuser = await _userManager.FindByNameAsync(user.Username);
            if (appuser == null || user == null) return NotFound("User does not exist");
            user.Status = "Active";
            user.Password = DateTime.Now.ToString("ddhhmmss");
            appuser.EmailConfirmed = false;
            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _userManager.RemovePasswordAsync(appuser);
                await _userManager.AddPasswordAsync(appuser, user.Password);
                string passwordhash = _userManager.PasswordHasher.HashPassword(appuser, user.Password);
                appuser.PasswordHash = passwordhash;
                await _userManager.UpdateNormalizedEmailAsync(appuser);
                await _userManager.UpdateNormalizedUserNameAsync(appuser);
                await _userManager.UpdateSecurityStampAsync(appuser);
                await _userManager.UpdateAsync(appuser);
                await _context.SaveChangesAsync();
                
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(appuser);
                var callbackUrl = Url.EmailConfirmationLink(appuser.Id, code, Request.Scheme);
                await _emailSender.SendEmailResetConfirmationAsync(user.Email, callbackUrl, user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok( new { Status = "Ok", Message = "Password Reset Successfully", NewPassword = user.Password });
        }

        [AllowAnonymous]
        [HttpGet("Reset/{email}")]
        public async Task<IActionResult> PassReset([FromRoute] string email)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = _context.Users.FirstOrDefault(a => a.Email == email);
            var appuser = await _userManager.FindByNameAsync(user.Username);
            if (appuser == null || user == null) return NotFound("User does not exist");
            appuser.EmailConfirmed = true;
            user.Password = DateTime.Now.ToString("ddhhmmss");

            try
            {
                await _userManager.RemovePasswordAsync(appuser);
                await _userManager.AddPasswordAsync(appuser, user.Password);
                string passwordhash = _userManager.PasswordHasher.HashPassword(appuser, user.Password);
                appuser.PasswordHash = passwordhash;
                await _userManager.UpdateNormalizedEmailAsync(appuser);
                await _userManager.UpdateNormalizedUserNameAsync(appuser);
                await _userManager.UpdateSecurityStampAsync(appuser);
                await _userManager.UpdateAsync(appuser);
                await _context.SaveChangesAsync();
                
                await _emailSender.SendEmailResetConfirmAsync(user.Email, user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(user.UserId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(new { Status = "Ok", Message = "Account Reset Successfully", Output = user });
        }

        [AllowAnonymous]
        [HttpPost("Reset")]
        public async Task<IActionResult> PassWordReset([FromBody] Reset reset)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = _context.Users.FirstOrDefault(a => a.Email == reset.Email);
            var appuser = await _userManager.FindByNameAsync(user.Username);
            if (appuser == null || user == null) return NotFound("User does not exist");
            user.Password = DateTime.Now.ToString("ddhhmmss");
            appuser.EmailConfirmed = true;

            try
            {
                await _userManager.RemovePasswordAsync(appuser);
                await _userManager.AddPasswordAsync(appuser, user.Password);
                string passwordhash = _userManager.PasswordHasher.HashPassword(appuser, user.Password);
                appuser.PasswordHash = passwordhash;
                await _userManager.UpdateNormalizedEmailAsync(appuser);
                await _userManager.UpdateNormalizedUserNameAsync(appuser);
                await _userManager.UpdateSecurityStampAsync(appuser);
                await _userManager.UpdateAsync(appuser);
                await _context.SaveChangesAsync();

                await _emailSender.SendEmailResetConfirmAsync(user.Email, user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(user.UserId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(new { Status = "Ok", Message = "Account Reset Successfully", Output = "Check Mail for new Password" });
        }

        // PUT: api/Users/5
        [HttpPost("ProfileImage/{id}")]
        public async Task<IActionResult> PutImageUsers([FromRoute] int id, [FromBody] Image image)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            Users user = _context.Users.FirstOrDefault(a => a.UserId == id);
            if (user == null) return NotFound($"User with id {id} does not exist");

            user.Image = image.ProfileImage;
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(image);
        }

        [HttpGet("AccessToken")]
        [AllowAnonymous]
        public async Task<IActionResult> AccessToken()
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("qwertyuiopasdfghjklzxcvbnm123456"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: "http://localhost:50563",
                audience: "http://localhost:50563",
                expires: DateTime.Now.AddMinutes(10),
                signingCredentials: creds);

            return Ok(new { access_token = new JwtSecurityTokenHandler().WriteToken(token), expires_in_Minutes = 10 });
        }

        private bool UsersExists(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }


        public class Login
        {
            [Required]
            public string Username { get; set; }
            [Required]
            public string Password { get; set; }
        }

        public class Image
        {
            [Required]
            public string ProfileImage { get; set; }
        }
        
        public class Reset
        {
            [Required]
            public string Email { get; set; }
        }

        public class ChangePassword
        {
            [Required]
            public string Username { get; set; }
            [Required]
            public string OldPassword { get; set; }
            [Required]
            public string NewPassword { get; set; }
        }

    }
}