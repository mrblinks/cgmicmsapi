﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Todo")]
    public class TodoController : Controller
    {
        private readonly CmsDbContext _context;

        public TodoController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/Todo
        [HttpGet]
        public IEnumerable<Todo> GetTodo()
        {
            return _context.Todo;
        }

        // GET: api/Todo/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todo = await _context.Todo.SingleOrDefaultAsync(m => m.TodoId == id);

            if (todo == null)
            {
                return NotFound();
            }

            return Ok(todo);
        }

        // GET: api/Todo/5
        [HttpGet("User/{id}")]
        public async Task<IActionResult> GetUserTodo([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var date = DateTime.Now.ToString(@"yyyy-MM-dd");
            var todo = _context.Todo.Where(m => m.CreatedUserId == id && m.Date.ToString(@"yyyy-MM-dd") == date).OrderBy(a => a.Date);

            if (todo == null)
            {
                return NotFound();
            }

            return Ok(todo);
        }

        // PUT: api/Todo/Completed/5
        [AllowAnonymous]
        [HttpPut("Completed/{id}")]
        public async Task<IActionResult> PutTodo([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            Todo todo = await _context.Todo.SingleOrDefaultAsync(m => m.TodoId == id);
            if (todo == null) return BadRequest();
            todo.Status = "Completed";
            _context.Entry(todo).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(todo);
        }

        // PUT: api/Todo/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodo([FromRoute] int id, [FromBody] Todo todo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != todo.TodoId)
            {
                return BadRequest();
            }

            _context.Entry(todo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Todo
        [HttpPost]
        public async Task<IActionResult> PostTodo([FromBody] Todo todo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Todo.Add(todo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTodo", new { id = todo.TodoId }, todo);
        }

        // DELETE: api/Todo/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todo = await _context.Todo.SingleOrDefaultAsync(m => m.TodoId == id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Todo.Remove(todo);
            await _context.SaveChangesAsync();

            return Ok(todo);
        }

        private bool TodoExists(int id)
        {
            return _context.Todo.Any(e => e.TodoId == id);
        }
    }
}