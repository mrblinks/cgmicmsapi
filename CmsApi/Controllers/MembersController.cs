﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;
using CmsApi.MyServices;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Members")]
    public class MembersController : Controller
    {
        private readonly CmsDbContext _context;
        SequenceCode seq;

        public MembersController(CmsDbContext context)
        {
            _context = context;
            seq = new SequenceCode(context);
        }

        // GET: api/Members
        [HttpGet]
        public IEnumerable<Member> GetMembers()
        {
            return _context.Members.Include(g => g.Group).Include(g => g.Department)
                .Include(a=>a.Branch).Include(c=>c.Company);
        }

        // GET: api/Members/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMember([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var member = await _context.Members.SingleOrDefaultAsync(m => m.MemberId == id);

            if (member == null)
            {
                return NotFound();
            }

            return Ok(member);
        }

        // GET: api/Members/Company/5
        [HttpGet("Church/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetMembersbyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var members = _context.Members.Where(m => m.CompanyId == id).Include(g => g.Group)
                .Include(g => g.Department);
            if (members == null)
            {
                return NotFound();
            }
            return Ok(members);
        }

        // GET: api/Members/Branch/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetMembersbyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var members = _context.Members.Where(m => m.BranchId == id).Include(g => g.Group)
                .Include(g => g.Department);
            if (members == null)
            {
                return NotFound();
            }
            return Ok(members);
        }

        // GET: api/Members/Branch/5
        [HttpGet("{name}/{id}")]
        public async Task<IActionResult> GetMembersByGroup([FromRoute] string name, int id)
        {
            if (!ModelState.IsValid)return BadRequest(ModelState);
            var members = new List<Member>();
                if (name.ToLower() == "groups")
                {
                    var group = _context.Groups.FirstOrDefault(m => m.GroupId == id);
                    if (group == null) return NotFound();
                    members = _context.Members.Where(m => m.GroupId == group.GroupId).ToList();
                }
                else if (name.ToLower() == "departments")
                {
                    var depart = _context.Department.FirstOrDefault(m => m.DepartmentId == id);
                    if (depart == null) return NotFound();
                    members = _context.Members.Where(m => m.DepartmentId == depart.DepartmentId).ToList();
                }
            if (members == null)return NotFound();

            return Ok(members);
        }


        // PUT: api/Members/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMember([FromRoute] int id, [FromBody] Member member)
        {
            if (!ModelState.IsValid)return BadRequest(ModelState);
            if (id != member.MemberId) return BadRequest();
            if(member.MemberNumber == null)
            {
                member.MemberNumber = await seq.GetCode("Member") + member.BranchId;
            }
            
            _context.Entry(member).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MemberExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Members
        [HttpPost]
        public async Task<IActionResult> PostMember([FromBody] Member member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string snCode = await seq.GetCode("Member");
            member.MemberNumber = snCode+member.BranchId;
            member.CreatedDate = DateTime.UtcNow;
            _context.Members.Add(member);
            await _context.SaveChangesAsync();

            string msg = await seq.Sms(member);

         return Ok(new { Status = "OK", Message = "Successfully Added", Output = member, Sms = msg });
        }
        // POST: api/Members

        [HttpPost("Batch")]
        public async Task<IActionResult> PostBatchMember([FromBody] Data data)
        {
            if (!ModelState.IsValid)return BadRequest(ModelState);

            foreach(var a in data.data)
            {
                a.MemberNumber = await seq.GetCode("Member") + data.BranchId;
                a.Mobile = "0" + a.Mobile;
                a.BranchId = data.BranchId;
                a.CompanyId = data.CompanyId;
                a.CreatedUserId = data.CreatedUserId;
                a.CreatedDate = DateTime.UtcNow;
                _context.Members.Add(a);
                //await seq.Sms(a);
            }
           
            await _context.SaveChangesAsync();

            return Ok(new { Status = "OK", Message = "Successfully Added", Output = "Members has been added" });
        }

        // DELETE: api/Members/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMember([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var member = await _context.Members.SingleOrDefaultAsync(m => m.MemberId == id);
            if (member == null)
            {
                return NotFound();
            }

            _context.Members.Remove(member);
            await _context.SaveChangesAsync();

            return Ok(member);
        }

        private bool MemberExists(int id)
        {
            return _context.Members.Any(e => e.MemberId == id);
        }

        public class Data : BaseModel
        {
            public List<Member> data { get; set; }
        }

    }
}