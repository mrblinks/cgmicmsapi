﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/ChurchServices")]
    public class ChurchServicesController : Controller
    {
        private readonly CmsDbContext _context;

        public ChurchServicesController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/ChurchServices
        [HttpGet]
        public IEnumerable<ChurchServices> GetChurchServices()
        {
            return _context.ChurchServices.Include(a => a.Branch).Include(a => a.CompanyDetails);
        }

        // GET: api/ChurchServices/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetChurchServices([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var churchServices = await _context.ChurchServices.SingleOrDefaultAsync(m => m.ChurchServicesId == id);

            if (churchServices == null)
            {
                return NotFound();
            }

            return Ok(churchServices);
        }

        // GET: api/ChurchServices/Company/5
        [HttpGet("Church/{id}")]
        public async Task<IActionResult> GetServicesbyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var services = _context.ChurchServices.Where(m => m.CompanyId == id);
            if (services == null)
            {
                return NotFound();
            }

            return Ok(services);
        }

        // GET: api/ChurchServices/Branch/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetServicesbyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var service = _context.ChurchServices.Where(m => m.BranchId == id);
            if (service == null)
            {
                return NotFound();
            }
            return Ok(service);
        }

        // PUT: api/ChurchServices/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChurchServices([FromRoute] int id, [FromBody] ChurchServices churchServices)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != churchServices.ChurchServicesId)
            {
                return BadRequest();
            }

            _context.Entry(churchServices).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChurchServicesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ChurchServices
        [HttpPost]
        public async Task<IActionResult> PostChurchServices([FromBody] ChurchServices churchServices)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ChurchServices.Add(churchServices);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetChurchServices", new { id = churchServices.ChurchServicesId }, churchServices);
        }

        // DELETE: api/ChurchServices/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChurchServices([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var churchServices = await _context.ChurchServices.SingleOrDefaultAsync(m => m.ChurchServicesId == id);
            if (churchServices == null)
            {
                return NotFound();
            }

            _context.ChurchServices.Remove(churchServices);
            await _context.SaveChangesAsync();

            return Ok(churchServices);
        }

        private bool ChurchServicesExists(int id)
        {
            return _context.ChurchServices.Any(e => e.ChurchServicesId == id);
        }
    }
}