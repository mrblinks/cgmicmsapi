﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CmsApi.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Dashboard")]
    public class DashboardController : Controller
    {
        private readonly CmsDbContext _context;

        public DashboardController(CmsDbContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetChurchDetails([FromRoute] int id)
        {
            Users user = _context.Users.Where(m => m.UserId == id).Include(c => c.CompanyDetails)
                .Include(b=>b.Branch).FirstOrDefault();

            var cedis = _context.PaymentType.Where(a => a.Symbol.ToLower().Contains("ngn")).FirstOrDefault();
            var usd = _context.PaymentType.Where(a => a.Symbol.ToLower().Contains("usd")).FirstOrDefault();
            var gbp = _context.PaymentType.Where(a => a.Symbol.ToLower().Contains("gbp")).FirstOrDefault();
            Dashboard dashboard = new Dashboard();
            dashboard.Cedis = "0.00"; dashboard.Dollar = "0.00"; dashboard.Pound = "0.00";
            if (user.Privillege.ToLower() == "church")
            {
                dashboard.Members = (_context.Members.Where(a => a.CompanyId == user.CompanyId)).Count();
                dashboard.Male = (_context.Members.Where(a => a.CompanyId == user.CompanyId &&
                                  a.Gender.ToLower() == "male")).Count();
                dashboard.Female = (_context.Members.Where(a => a.CompanyId == user.CompanyId &&
                                  a.Gender.ToLower() == "female")).Count();
                dashboard.Transaction = (_context.Transactions.Where(a => a.CompanyId == user.CompanyId)).Count();
                dashboard.Event = (_context.Events.Where(a => a.CompanyId == user.CompanyId &&
                                  a.Status.ToLower() == "upcoming")).Count();
                dashboard.Dairy = (_context.Dairy.Where(a => a.CreatedUserId == user.UserId)).Count();
                if (cedis != null)
                {
                    decimal ced = (_context.GeneralLedgerCodes.Where(a => a.CompanyId == user.CompanyId &&
                                      a.CurrencyId == cedis.CurrencyId && a.GlType.ToLower() == "cashbook").Select(a => a.Balance)).Sum();
                    dashboard.Cedis = String.Format("{0:0.00}", ced);
                }
                if (usd != null)
                {
                    decimal dol = (_context.GeneralLedgerCodes.Where(a => a.CompanyId == user.CompanyId &&
                                  a.CurrencyId == usd.CurrencyId && a.GlType.ToLower() == "cashbook").Select(a => a.Balance)).Sum();
                    dashboard.Dollar = String.Format("{0:0.00}", dol);
                }
                if (gbp != null)
                {
                    decimal pound = (_context.GeneralLedgerCodes.Where(a => a.CompanyId == user.CompanyId &&
                                  a.CurrencyId == gbp.CurrencyId && a.GlType.ToLower() == "cashbook").Select(a => a.Balance)).Sum();
                    dashboard.Pound = String.Format("{0:0.00}", pound);
                }
            }
            else
            {
                dashboard.Members = (_context.Members.Where(a => a.BranchId == user.BranchId)).Count();
                dashboard.Male = (_context.Members.Where(a => a.BranchId == user.BranchId &&
                                  a.Gender.ToLower() == "male")).Count();
                dashboard.Female = (_context.Members.Where(a => a.BranchId == user.BranchId &&
                                  a.Gender.ToLower() == "female")).Count();
                dashboard.Transaction = (_context.Transactions.Where(a => a.BranchId == user.BranchId)).Count();
                dashboard.Event = (_context.Events.Where(a => a.BranchId == user.BranchId &&
                                  a.Status.ToLower() == "upcoming")).Count();
                dashboard.Dairy = (_context.Dairy.Where(a => a.CreatedUserId == user.UserId)).Count();

                if (cedis != null)
                {
                    decimal ced = (_context.GeneralLedgerCodes.Where(a => a.BranchId == user.BranchId &&
                                      a.CurrencyId == cedis.CurrencyId && a.GlType.ToLower() == "cashbook").Select(a => a.Balance)).Sum();
                    dashboard.Cedis = String.Format("{0:0.00}", ced);
                }
                if (usd != null)
                {
                    decimal dol = (_context.GeneralLedgerCodes.Where(a => a.BranchId == user.BranchId &&
                                  a.CurrencyId == usd.CurrencyId && a.GlType.ToLower() == "cashbook").Select(a => a.Balance)).Sum();
                    dashboard.Dollar = String.Format("{0:0.00}", dol);
                }
                if (gbp != null)
                {
                    decimal pound = (_context.GeneralLedgerCodes.Where(a => a.BranchId == user.BranchId &&
                                  a.CurrencyId == gbp.CurrencyId && a.GlType.ToLower() == "cashbook").Select(a => a.Balance)).Sum();
                    dashboard.Pound = String.Format("{0:0.00}", pound);
                }
            }

            dashboard.Todo = (_context.Todo.Where(a => a.CreatedUserId == user.UserId)).Count();
            dashboard.Fullname = user.FullName;
            dashboard.Email = user.Email;
            dashboard.Mobile = user.Phone;
            dashboard.UserType = user.UserType;
            dashboard.Username = user.Username;
            dashboard.Image = user.Image;
            dashboard.Branch = user.Branch.BranchName;
            dashboard.MTodo = (_context.Todo.Where(m => m.CreatedUserId == user.UserId
            && m.Date.ToString("MM") == DateTime.Now.ToString("MM"))).Count();
            dashboard.CTodo = (_context.Todo.Where(m => m.CreatedUserId == user.UserId && m.Status.ToLower() != "pending"
            && m.Date.ToString("MM") == DateTime.Now.ToString("MM"))).Count();
            dashboard.UTodo = (_context.Todo.Where(m => m.CreatedUserId == user.UserId && m.Status.ToLower() == "pending"
            && m.Date.ToString("MM") == DateTime.Now.ToString("MM"))).Count();
            
            dashboard.Today = (_context.Todo.Where(m => m.CreatedUserId == user.UserId && m.Status.ToLower().Contains("pending") 
            && m.Date <= DateTime.Now).OrderBy(a => a.Date)).ToList();
            
            return Ok(dashboard);
        }

        [HttpGet("Admin/{id}")]
        public async Task<IActionResult> GetAppDetail([FromRoute] int id)
        {
            Users user = _context.Users.Where(m => m.UserId == id).Include(c => c.CompanyDetails)
                .Include(b => b.Branch).FirstOrDefault();

            var cedis = _context.PaymentType.Where(a => a.Symbol.ToLower().Contains("ngn")).FirstOrDefault();
            var usd = _context.PaymentType.Where(a => a.Symbol.ToLower().Contains("usd")).FirstOrDefault();
            var gbp = _context.PaymentType.Where(a => a.Symbol.ToLower().Contains("gbp")).FirstOrDefault();
            AdminDashboard dashboard = new AdminDashboard();
            dashboard.Cedis = "0.00"; dashboard.Dollar = "0.00"; dashboard.Pound = "0.00";
            dashboard.Members = (_context.Members).Count();
            dashboard.Male = (_context.Members.Where(a=>a.Gender.ToLower() == "male")).Count();
            dashboard.Female = (_context.Members.Where(a=>a.Gender.ToLower() == "female")).Count();
            dashboard.Transaction = (_context.Transactions).Count();
            dashboard.Church = (_context.CompanyDetails).Count();
            dashboard.Branchs = (_context.BranchDetails).Count();
            dashboard.Users = (_context.Users).Count();
            dashboard.Groups = (_context.Groups).Count();
            dashboard.Department = (_context.Department).Count();
            dashboard.Smslog = (_context.Smslog).Count();
            dashboard.SmsApi = (_context.SmsApi).Count();
            dashboard.Schedule = (_context.Schedule).Count();
            dashboard.Message = (_context.Message).Count();
            dashboard.Nominal = (_context.GeneralLedgerCodes).Count();
            dashboard.Licence = (_context.LicenceLog).Count();
            dashboard.Budget = (_context.Budget).Count();
            dashboard.Event = (_context.Events.Where(a=>a.Status.ToLower().Contains("upcoming"))).Count();
            dashboard.Dairy = (_context.Dairy.Where(a => a.CreatedUserId == user.UserId)).Count();
            dashboard.Todo = (_context.Todo.Where(a => a.CreatedUserId == user.UserId)).Count();
            dashboard.Dairy = (_context.Dairy.Where(a => a.CreatedUserId == user.UserId)).Count();
            dashboard.Todo = (_context.Todo.Where(a => a.CreatedUserId == user.UserId)).Count();
            if (cedis != null)
            {
                decimal ced = (_context.GeneralLedgerCodes.Where(a=>a.CurrencyId == cedis.CurrencyId && 
                            a.GlType.ToLower().Contains("Cashbook")).Select(a => a.Balance)).Sum();
                dashboard.Cedis = String.Format("{0:0.00}", ced);
            }
            if (usd != null)
            {
                decimal dol = (_context.GeneralLedgerCodes.Where(a =>a.CurrencyId == usd.CurrencyId &&
                            a.GlType.ToLower().Contains("Cashbook")).Select(a => a.Balance)).Sum();
                dashboard.Dollar = String.Format("{0:0.00}", dol);
            }
            if (gbp != null)
            {
                decimal pound = (_context.GeneralLedgerCodes.Where(a=>a.CurrencyId == gbp.CurrencyId &&
                            a.GlType.ToLower().Contains("Cashbook")).Select(a => a.Balance)).Sum();
                dashboard.Pound = String.Format("{0:0.00}", pound);
            }
            dashboard.Fullname = user.FullName;
            dashboard.Email = user.Email;
            dashboard.Mobile = user.Phone;
            dashboard.UserType = user.UserType;
            dashboard.Username = user.Username;
            dashboard.Image = user.Image;
            dashboard.Branch = user.Branch.BranchName;
            dashboard.MTodo = (_context.Todo.Where(m => m.CreatedUserId == user.UserId
            && m.Date.ToString("MM") == DateTime.Now.ToString("MM"))).Count();
            dashboard.CTodo = (_context.Todo.Where(m => m.CreatedUserId == user.UserId && m.Status.ToLower() != "pending"
            && m.Date.ToString("MM") == DateTime.Now.ToString("MM"))).Count();
            dashboard.UTodo = (_context.Todo.Where(m => m.CreatedUserId == user.UserId && m.Status.ToLower() == "pending"
            && m.Date.ToString("MM") == DateTime.Now.ToString("MM"))).Count();

            dashboard.Today = (_context.Todo.Where(m => m.CreatedUserId == user.UserId && m.Date <= DateTime.Now &&
            m.Status.ToLower().Contains("pending")).OrderBy(a => a.Date)).ToList();
            
            return Ok(dashboard);
        }
        

        public class Dashboard
        {
            public int Members { get; set; }
            public int Male { get; set; }
            public int Female { get; set; }
            public int Transaction { get; set; }
            public int Event { get; set; }
            public int Dairy { get; set; }
            public int Todo { get; set; }
            public string Cedis { get; set; }
            public string Dollar { get; set; }
            public string Pound { get; set; }
            public string Fullname { get; set; }
            public string Email { get; set; }
            public string Image { get; set; }
            public string Mobile { get; set; }
            public string UserType { get; set; }
            public string Username { get; set; }
            public string Branch { get; set; }
            public int MTodo { get; set; }
            public int CTodo { get; set; }
            public int UTodo { get; set; }
            public List<Todo> Today { get; set; }
        }

        public class AdminDashboard
        {
            public int Members { get; set; }
            public int Male { get; set; }
            public int Female { get; set; }
            public int Transaction { get; set; }
            public int Event { get; set; }
            public int Dairy { get; set; }
            public int Todo { get; set; }
            public int Groups { get; set; }
            public int Department { get; set; }
            public int Nominal { get; set; }
            public int Budget { get; set; }
            public int Smslog { get; set; }
            public int SmsApi { get; set; }
            public int Message { get; set; }
            public int Schedule { get; set; }
            public int Licence { get; set; }
            public int Branchs { get; set; }
            public int Church { get; set; }
            public int Users { get; set; }
            public string Cedis { get; set; }
            public string Dollar { get; set; }
            public string Pound { get; set; }
            public string Fullname { get; set; }
            public string Email { get; set; }
            public string Image { get; set; }
            public string Mobile { get; set; }
            public string UserType { get; set; }
            public string Username { get; set; }
            public string Branch { get; set; }
            public int MTodo { get; set; }
            public int CTodo { get; set; }
            public int UTodo { get; set; }
            public List<Todo> Today { get; set; }
        }
    }
}