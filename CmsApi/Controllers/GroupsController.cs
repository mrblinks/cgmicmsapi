﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Groups")]
    public class GroupsController : Controller
    {
        private readonly CmsDbContext _context;

        public GroupsController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/Groups
        [HttpGet]
        public IEnumerable<Groups> GetGroups()
        {
            return _context.Groups.Include(a => a.Branch).Include(a => a.CompanyDetails);
        }

        // GET: api/Groups/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGroups([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var groups = await _context.Groups.SingleOrDefaultAsync(m => m.GroupId == id);

            if (groups == null)
            {
                return NotFound();
            }

            return Ok(groups);
        }

        // GET: api/GeneralLedgerCodes/Company/5
        [HttpGet("Church/{id}")]
        public async Task<IActionResult> GetGroupbyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var group = _context.Groups.Where(m => m.CompanyId == id);
            if (group == null)
            {
                return NotFound();
            }

            return Ok(group);
        }

        // GET: api/GeneralLedgerCodes/Branch/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetGroupbyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var group = _context.Groups.Where(m => m.BranchId == id);
            if (group == null)
            {
                return NotFound();
            }
            return Ok(group);
        }

        // PUT: api/Groups/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGroups([FromRoute] int id, [FromBody] Groups groups)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != groups.GroupId)
            {
                return BadRequest();
            }
            groups.CreatedDate = DateTime.Now;
            _context.Entry(groups).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Groups
        [HttpPost]
        public async Task<IActionResult> PostGroups([FromBody] Groups groups)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Groups.Add(groups);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGroups", new { id = groups.GroupId }, groups);
        }

        [HttpPost("Batch")]
        public async Task<IActionResult> PostBatchGroup([FromBody] Data data)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            foreach (var a in data.data)
            {
                a.BranchId = data.BranchId;
                a.CompanyId = data.CompanyId;
                a.CreatedUserId = data.CreatedUserId;
                a.CreatedDate = DateTime.UtcNow;
                _context.Groups.Add(a);
                //await seq.Sms(a);
            }

            await _context.SaveChangesAsync();
            return Ok(new { Status = "OK", Message = "Successfully Added", Output = "Groups has been added" });
        }


        // DELETE: api/Groups/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGroups([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var groups = await _context.Groups.SingleOrDefaultAsync(m => m.GroupId == id);
            if (groups == null)
            {
                return NotFound();
            }

            _context.Groups.Remove(groups);
            await _context.SaveChangesAsync();

            return Ok(groups);
        }

        private bool GroupsExists(int id)
        {
            return _context.Groups.Any(e => e.GroupId == id);
        }

        public class Data : BaseModel
        {
            public List<Groups> data { get; set; }
        }
    }
}