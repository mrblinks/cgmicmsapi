﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CmsApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.ComponentModel.DataAnnotations;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Report")]
    public class ReportController : Controller
    {
        private readonly CmsDbContext _context;

        public ReportController(CmsDbContext context)
        {
            _context = context;
        }

        // POST: api/Report/
        [HttpPost]
        public async Task<IActionResult> PostTransaction([FromBody] Report report)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                var m = report.Start; var b = report.End; var r = report.Id;
                var transaction = _context.Transactions.
                                  Where(a => a.CreatedDate >= m && a.CreatedDate <= b && a.CompanyId == report.CompanyId && a.BranchId == report.BranchId)
                                  .Include(a => a.PaymentMethod)
                                  .Include(a => a.Currency).Include(a => a.GetGeneralLedgerCode);

                if (transaction == null) return NotFound($"No Transaction {report.Id} For Companys");

                return Ok(transaction);
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }

        // POST: api/Report/Enquiry
        [HttpPost("Enquiry")]
        public async Task<IActionResult> PostEnquiryTransaction([FromBody] Report report)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var gl = _context.GeneralLedgerCodes.Where(a => a.GeneralLedgerCodeId == report.Id );
            if (gl == null) return NotFound($"Payment Type with id {report.Id} does not exist");

            try
            {
                var m = report.Start; var b = report.End; var r = report.Id;
                var transaction = _context.Transactions.
                                  Where(a => a.CreatedDate >= m && a.CreatedDate <= b && a.GeneralLedgerCodeId == r)
                                  .Include(a => a.PaymentMethod).Include(a => a.Currency);
                if (transaction == null) return NotFound($"There is no Transaction for {report.Id} among Transactions");

                return Ok(transaction);
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }

        // POST: api/Reports/Member
        [HttpPost("Member")]
        public async Task<IActionResult> PostMemberTransaction([FromBody] Report report)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var member = _context.Members.Where(a => a.MemberId == report.Id );
            if (member == null) return NotFound($"Member with id {report.Id} does not exist");
            try
            {
                var m = report.Start; var b = report.End; var r = report.Id;
                var transaction = _context.MenTransactions.
                                  Where(a => a.CreatedDate >= m && a.CreatedDate <= b && a.MemberId == r)
                                  .Include(a => a.Transaction).Include(a => a.Transaction.PaymentMethod)
                                  .Include(a => a.Transaction.Currency).Include(a => a.Transaction.GetGeneralLedgerCode);

                if (transaction == null) return NotFound($"There is no Transaction for member with id {report.Id} in Database");

                return Ok(transaction);
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }

        // POST: api/Reports/Member
        [HttpPost("Account")]
        public async Task<IActionResult> PostAccountTransaction([FromBody] Report report)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var account = _context.GeneralLedgerCodes.Where(a => a.MainGeneralLedgerCodeId == report.Id);
            if (account == null) return NotFound($"Account with id {report.Id} does not exist");
            try
            {
                var m = report.Start; var b = report.End; var r = report.Id;
                var transaction = _context.AcctTransactions.
                                  Where(a => a.CreatedDate >= m && a.CreatedDate <= b && a.GeneralLedgerCodeId == r)
                                  .Include(a => a.Transaction).Include(a => a.Transaction.PaymentMethod)
                                  .Include(a => a.Transaction.Currency).Include(a => a.Transaction.GetGeneralLedgerCode);

                if (transaction == null) return NotFound($"There is no Transaction for Account with id {report.Id} in Database");

                return Ok(transaction);
            }
            catch (DbUpdateConcurrencyException e)
            {
                StringContent sc = new StringContent(e.ToString());
                return NoContent();
            }

        }
        
        [HttpPost("GenerateTrialBal")]
        public IActionResult Gen([FromBody] Report model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var trialbalance = new List<TrialModel>();
            var curr = _context.PaymentType.Where(a => a.Symbol.ToLower().Contains("ghc")).FirstOrDefault();
            var nominalCodes = (_context.Transactions.Where(a => a.BranchId == model.Id && a.CurrencyId == curr.CurrencyId)
                .Select(a => a.GeneralLedgerCodeId)).Distinct().ToList();
            for (int i = 0; i < nominalCodes.Count; i++)
            {
                var debit = (_context.Transactions.Where(a =>
                    a.GeneralLedgerCodeId == nominalCodes[i] && a.CreatedDate > model.Start && 
                    a.TransType.ToLower().Contains("debit") && a.CreatedDate < model.End).Select(a => a.Amount)).Sum();
                var credit = (_context.Transactions.Where(a =>
                    a.GeneralLedgerCodeId == nominalCodes[i] && a.CreatedDate > model.Start &&
                    a.TransType.ToLower().Contains("credit") && a.CreatedDate < model.End).Select(a => a.Amount)).Sum();
                var openingbal = ((_context.Transactions.Where(a =>
                                         a.GeneralLedgerCodeId == nominalCodes[i] && a.TransType.ToLower().Contains("dedit") && 
                                         a.CreatedDate < model.Start).Select(a => a.Amount)).Sum())
                                 - ((_context.Transactions.Where(a =>
                                         a.GeneralLedgerCodeId == nominalCodes[i] && a.TransType.ToLower().Contains("credit") && 
                                         a.CreatedDate < model.Start).Select(a => a.Amount)).Sum());
                trialbalance.Add(
                    new TrialModel
                    {
                        OpeningBalance= (openingbal == 0 ? openingbal +"" : (openingbal < 0 ? openingbal * (-1) +"" : openingbal +"")),
                        Debits= String.Format("{0:0.00}", debit),
                        Credits= String.Format("{0:0.00}", credit),
                        Description= ((_context.GeneralLedgerCodes.Where(a => a.GeneralLedgerCodeId == nominalCodes[i])
                    .Select(a => a.Description)).FirstOrDefault()),
                        ClosingBalance = ((debit > credit) ? (debit - credit) + " Dr" : (credit - debit) + " Cr")
                    });
                
            }
            return Ok(trialbalance);
        }

        public class TrialModel
        {
            public string Description { get; set; }
            public string OpeningBalance { get; set; }
            public string Debits { get; set; }
            public string Credits { get; set; }
            public string ClosingBalance { get; set; }
        }

        public class Report : BaseModel
        {
            [Required]
            public int Id { get; set; }
            public int CurrencyId { get; set; }
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
        }
    }
}