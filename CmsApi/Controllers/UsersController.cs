﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;
using CmsApi.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using CmsApi.Extensions;
using CmsApi.MyServices;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly CmsDbContext _context;
        private readonly IEmailSender _emailSender;

        public UsersController(UserManager<ApplicationUser> usrMgr, CmsDbContext context, IEmailSender emailSender)
        {
            _userManager = usrMgr;
            _context = context;
            _emailSender = emailSender;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<Users> GetUsers()
        {
            return _context.Users.Include(b => b.Branch);
        }

        // GET: api/Users
        [HttpGet("App")]
        public IEnumerable<ApplicationUser> GetAppUsers()
        {
            return _userManager.Users;
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUsers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = _context.Users.Where(m => m.UserId == id).FirstOrDefault();
                //.Include(c=>c.CompanyDetails).FirstOrDefault();

            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        //GET: api/Users/uname
        [HttpGet("{uname:alpha}")]
        public IActionResult GetUser([FromRoute] string uname)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var usr = _context.Users.Any(u => u.Username.Equals(uname));
            if (!usr)
            {
                return NotFound("User not found");
            }
            return Ok(_context.Users.Where(u => u.Username.ToLower() == uname));
        }

        // GET: api/Users/Branch/5
        [HttpGet("Church/{id}")]
        public async Task<IActionResult> GetUsersbyCompanyId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var users = _context.Users.Where(m => m.CompanyId == id);
            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        // GET: api/Users/Branch/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetUsersbyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var users = _context.Users.Where(m => m.BranchId == id);
            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsers([FromRoute] int id, [FromBody] Users users)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != users.UserId) return BadRequest();

            _context.Entry(users).State = EntityState.Modified;
            try
            {
                ApplicationUser appUser = await _userManager.FindByNameAsync(users.Username);
                appUser.Email = users.Email;
                appUser.PasswordHash = appUser.PasswordHash;
                await _userManager.UpdateNormalizedEmailAsync(appUser);
                await _userManager.UpdateNormalizedUserNameAsync(appUser);
                await _userManager.UpdateSecurityStampAsync(appUser);
                await _userManager.UpdateAsync(appUser);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPut("Disable/{id}")]
        public async Task<IActionResult> PutStatusUsers([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            Users users = await _context.Users.SingleOrDefaultAsync(m => m.UserId == id);

            if (users == null) return BadRequest();
            users.Status = "Disabled";
            _context.Entry(users).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok("User Has been Deactivated");
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostUsers([FromBody] Users user)
        {
            if (!ModelState.IsValid)return BadRequest(ModelState);
            
            var usr = _context.Users.Any(u => u.Username.Equals(user.Username));
            var appusr = _userManager.Users.Any(u => u.UserName.Equals(user.Username));
            var appemail = _userManager.Users.Any(u => u.Email.Equals(user.Email));
            if (usr || appusr || appemail)
            {
                return BadRequest("Username or Email already taken");
            }
            user.LastLogin = DateTime.Now;
            user.LastLogOut = DateTime.Now;
            user.CreatedDate = DateTime.Now;
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            ApplicationUser appUser = new ApplicationUser
            {
                UserName = user.Username,
                Email = user.Email
            };
            IdentityResult result = await _userManager.CreateAsync(appUser, user.Password);

            if (!result.Succeeded)
            {
                return BadRequest("Incorrect Username or password");
            }

            var code = await _userManager.GenerateEmailConfirmationTokenAsync(appUser);
            var callbackUrl = Url.EmailConfirmationLink(appUser.Id, code, Request.Scheme);
            await _emailSender.SendEmailConfirmationAsync(user.Email, callbackUrl, user);

            return Ok(new { Status = "OK", Message = "Successfully Added", Output = "User has been added", User = user });
            
        }
        
        //DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUsers([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var users = await _context.Users.SingleOrDefaultAsync(m => m.UserId == id);
            if (users == null)
            {
                return NotFound();
            }
            
            _context.Users.Remove(users);
            await _context.SaveChangesAsync();
            ApplicationUser appUser = await _userManager.FindByNameAsync(users.Username);
            IdentityResult result = await _userManager.DeleteAsync(appUser);

            return Ok(users);
        }

        //DELETE: api/Users/uname
        [HttpDelete("Username/{uname}")]
        public async Task<IActionResult> DeleteIdentityUsers([FromRoute] string uname)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            //string email = "Charles.Asante";
            ApplicationUser appUser = await _userManager.FindByNameAsync(uname);
            if (appUser == null)
            {
                return NotFound();
            }
            IdentityResult result = await _userManager.DeleteAsync(appUser);

            return Ok(appUser);
        }

        private bool UsersExists(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }
        
    }
}