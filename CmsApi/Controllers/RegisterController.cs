﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using CmsApi.Models;
using Microsoft.AspNetCore.Identity;
using CmsApi.Models.Identity;
using CmsApi.MyServices;
using CmsApi.Extensions;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Register")]
    public class RegisterController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly CmsDbContext _context;
        private readonly IEmailSender _emailSender;
        SequenceCode seq;

        public RegisterController(UserManager<ApplicationUser> usrMgr, CmsDbContext context, IEmailSender emailSender)
        {
            _userManager = usrMgr;
            _context = context;
            _emailSender = emailSender;
            seq = new SequenceCode(context);
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostUsers([FromBody] Register register)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            CompanyDetails companyDetails = register.Church;
            BranchDetails branchDetails = register.Branch;
            Users user = register.User;
            var usr = _context.Users.Any(u => u.Username.Equals(user.Username));
            var appusr = _userManager.Users.Any(u => u.UserName.Equals(user.Username));
            var appemail = _userManager.Users.Any(u => u.Email.Equals(user.Email));
            if (usr || appusr || appemail)
            {
                return BadRequest("Username already taken");
            }

            companyDetails.CompanyNo = await seq.GetCode("Church");
            companyDetails.CreatedDate = DateTime.UtcNow;
            _context.CompanyDetails.Add(companyDetails);
            await _context.SaveChangesAsync();

            branchDetails.BranchCode = await seq.GetCode("Branch") + companyDetails.CompanyId;
            branchDetails.CompanyId = companyDetails.CompanyId;
            branchDetails.Status = "Active";
            branchDetails.ExpiryDate = companyDetails.ExpiryDate;
            branchDetails.CreatedDate = DateTime.UtcNow;
            _context.BranchDetails.Add(branchDetails);
            await _context.SaveChangesAsync();

            user.CompanyId = companyDetails.CompanyId;
            user.BranchId = branchDetails.BranchId;
            user.Privillege = "Church";
            user.UserType = "Administrator";
            user.Status = "Active";
            user.CreatedDate = DateTime.UtcNow;
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            ApplicationUser appUser = new ApplicationUser
            {
                UserName = user.Username,
                Email = user.Email
            };
            IdentityResult result = await _userManager.CreateAsync(appUser, user.Password);
            if (!result.Succeeded)
            {
                return BadRequest("Incorrect Username or password");
            }
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(appUser);
            var callbackUrl = Url.EmailConfirmationLink(appUser.Id, code, Request.Scheme);
            await _emailSender.SendEmailConfirmationAsync(user.Email, callbackUrl, user);

            return Ok(new { Status = "OK", Message = "Successfully Added", Output = "Company has Been Registered Successfully", Data = register });
        }

        public class Register
        {
            [Required]
            public CompanyDetails Church { get; set; }
            [Required]
            public BranchDetails Branch { get; set; }
            [Required]
            public Users User { get; set; }
        }
    }
}