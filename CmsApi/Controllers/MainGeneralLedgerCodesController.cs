﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/MainGeneralLedgerCodes")]
    public class MainGeneralLedgerCodesController : Controller
    {
        private readonly CmsDbContext _context;

        public MainGeneralLedgerCodesController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/MainGeneralLedgerCodes
        [HttpGet]
        public IEnumerable<MainGeneralLedgerCodes> GetMainGeneralLedgerCodes()
        {
            return _context.MainGeneralLedgerCodes;
        }

        // GET: api/MainGeneralLedgerCodes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMainGeneralLedgerCodes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var mainGeneralLedgerCodes = await _context.MainGeneralLedgerCodes.SingleOrDefaultAsync(m => m.MainGeneralLedgerCodeId == id);
            if (mainGeneralLedgerCodes == null)
            {
                return NotFound();
            }
            return Ok(mainGeneralLedgerCodes);
        }

        //// GET: api/GeneralLedgerCodes/Company/5
        //[HttpGet("Company/{id}")]
        //public async Task<IActionResult> GetMGlcodebyCompanyId([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //    var generalLedgerCode = _context.MainGeneralLedgerCodes.Where(m => m.CompanyId == id);
        //    if (generalLedgerCode == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(generalLedgerCode);
        //}


        // PUT: api/MainGeneralLedgerCodes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMainGeneralLedgerCodes([FromRoute] int id, [FromBody] MainGeneralLedgerCodes mainGeneralLedgerCodes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mainGeneralLedgerCodes.MainGeneralLedgerCodeId)
            {
                return BadRequest();
            }

            _context.Entry(mainGeneralLedgerCodes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MainGeneralLedgerCodesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MainGeneralLedgerCodes
        [HttpPost]
        public async Task<IActionResult> PostMainGeneralLedgerCodes([FromBody] MainGeneralLedgerCodes mainGeneralLedgerCodes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MainGeneralLedgerCodes.Add(mainGeneralLedgerCodes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMainGeneralLedgerCodes", new { id = mainGeneralLedgerCodes.MainGeneralLedgerCodeId }, mainGeneralLedgerCodes);
        }

        // DELETE: api/MainGeneralLedgerCodes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMainGeneralLedgerCodes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var mainGeneralLedgerCodes = await _context.MainGeneralLedgerCodes.SingleOrDefaultAsync(m => m.MainGeneralLedgerCodeId == id);
            if (mainGeneralLedgerCodes == null)
            {
                return NotFound();
            }

            _context.MainGeneralLedgerCodes.Remove(mainGeneralLedgerCodes);
            await _context.SaveChangesAsync();

            return Ok(mainGeneralLedgerCodes);
        }

        private bool MainGeneralLedgerCodesExists(int id)
        {
            return _context.MainGeneralLedgerCodes.Any(e => e.MainGeneralLedgerCodeId == id);
        }
    }
}