﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CmsApi.Models;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Sms")]
    public class SmsController : Controller
    {
        private readonly CmsDbContext _context;

        public SmsController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/Smslog
        [HttpGet]
        public IEnumerable<Smslog> GetSmslog()
        {
            return _context.Smslog.Include(a => a.Branch).Include(a => a.Company);
        }

        [HttpPost]
        public async Task<IActionResult> Sms([FromBody] SmsModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            SmsApi smsconfig = _context.SmsApi.LastOrDefault(a => a.Status.ToLower().Contains("active") && a.BranchId == model.BranchId);
            if (smsconfig == null) return Content("No Sms Configuration Found");
            
            else
            {
                Smslog smslog = new Smslog();
                smslog.CreatedUserId = model.CreatedUserId; smslog.BranchId = model.BranchId;
                smslog.CompanyId = model.CompanyId; smslog.CreatedDate = DateTime.Now;
                smslog.Destination = model.Destination; smslog.Message = model.Message;
                model.Message = model.Message.Replace(" ", "+");
                try
                {
                    var httpClient = new HttpClient();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(smsconfig.Url).Append("?cmd=sendquickmsg&owneremail=Harmonizerblinks@gmail.com&subacct=")
                        .Append(smsconfig.Username).Append("&subacctpwd=").Append(smsconfig.Password)
                        .Append("&message=").Append(model.Message).Append("&sender=").Append(smsconfig.SenderId)
                        .Append("&sendto=").Append(model.Destination).Append("&msgtype=0");
                    var json = await httpClient.GetStringAsync(sb.ToString());
                    //var smsresponse = JsonConvert.DeserializeObject<SmsResponse>(json);
                    smslog.Response = json;
                    _context.Smslog.Add(smslog);
                    await _context.SaveChangesAsync();
                    if (json.Contains("ERR"))
                    {
                        return BadRequest(new { Response = json, Message = "Not Successfull" });
                    }
                    else if (json.Contains("OK"))
                    {
                        return Ok(new { Response = json, Message = "Successfully" });
                    }
                return NoContent();
                }
                catch (Exception e)
                {
                    return Content(e.Message);
                }
            }
        }

        [AllowAnonymous]
        [HttpGet("Birthday")]
        public async Task<IActionResult> BirthdaySms()
        {
            List<Member> model = _context.Members.Where(a=>a.DateOfBirth.ToString(@"MM-dd") == DateTime.Now.ToString(@"MM-dd")).ToList();
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].DateOfBirth.ToString(@"MM-dd") == DateTime.Now.ToString(@"MM-dd"))
                {
                    //CompanyDetails church = _context.CompanyDetails.Where(a => a.CompanyId == model[i].CompanyId).LastOrDefault();
                    //BranchDetails branch = _context.BranchDetails.Where(a => a.BranchId == model[i].BranchId).LastOrDefault();
                    SmsApi smsconfig = _context.SmsApi.LastOrDefault(a=>a.Status.ToLower().Contains("active") &&
                            a.BranchId == model[i].BranchId);
                    Message msg = _context.Message.LastOrDefault(a => a.Type.ToLower().Contains("birthday") &&
                             a.BranchId == model[i].BranchId);
                    if (smsconfig != null)
                    {
                        SmsModel sms = new SmsModel();
                        sms.Destination = model[i].Mobile;
                        sms.Message = msg.Messages;
                        Smslog smslog = new Smslog();
                        smslog.CreatedUserId = model[i].CreatedUserId; smslog.BranchId = model[i].BranchId;
                        smslog.CompanyId = model[i].CompanyId; smslog.CreatedDate = DateTime.Now;
                        smslog.Destination = sms.Destination; smslog.Message = sms.Message;
                        try
                        {
                            var httpClient = new HttpClient();
                            StringBuilder sb = new StringBuilder();
                            sb.Append(smsconfig.Url).Append("?cmd=sendquickmsg&owneremail=Harmonizerblinks@gmail.com&subacct=")
                                .Append(smsconfig.Username).Append("&subacctpwd=").Append(smsconfig.Password)
                                .Append("&message=").Append(sms.Message).Append("&sender=").Append(smsconfig.SenderId)
                                .Append("&sendto=").Append(sms.Destination).Append("&msgtype=0");

                            var json = await httpClient.GetStringAsync(sb.ToString());
                            //var smsresponse = JsonConvert.DeserializeObject<SmsResponse>(json);
                            smslog.Response = json;
                            _context.Smslog.Add(smslog);
                            await _context.SaveChangesAsync();
                            //return Ok(new { Response = json, Message = "Sent Successfully" });
                        }
                        catch (Exception e)
                        {
                            return Content(e.Message);
                        }
                    }
                }
            }
            return Ok(new { Status = "OK", Message = "Successfully Sent" });
        }

        [AllowAnonymous]
        [HttpGet("Schedule")]
        public async Task<IActionResult> ScheduleSms()
        {
            List<Schedule> model = _context.Schedule.Where(a => a.Date.ToString(@"yyyy-MM-dd") == DateTime.Now.ToString(@"yyyy-MM-dd")).ToList();
            //List<Member> member = _context.Members.ToList();
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].Date.ToString(@"yyyy-MM-dd") == DateTime.Now.ToString(@"yyyy-MM-dd"))
                {
                    model[i].Status = "Started";
                    SmsApi smsconfig = _context.SmsApi.LastOrDefault(a => a.Status.ToLower().Contains("active") &&
                        a.BranchId == model[i].BranchId);
                    if (smsconfig != null)
                    {
                        SmsModel sms = new SmsModel();
                        sms.Destination = model[i].Destination;
                        sms.Message = model[i].Message; 
                        Smslog smslog = new Smslog();
                        smslog.CreatedUserId = model[i].CreatedUserId; smslog.BranchId = model[i].BranchId;
                        smslog.CompanyId = model[i].CompanyId; smslog.CreatedDate = DateTime.Now;
                        smslog.Destination = model[i].Destination; smslog.Message = model[i].Message;
                        smslog.ScheduleId = model[i].ScheduleId;
                        try
                        {
                            var httpClient = new HttpClient();
                            StringBuilder sb = new StringBuilder();
                            sb.Append(smsconfig.Url).Append("?cmd=sendquickmsg&owneremail=Harmonizerblinks@gmail.com&subacct=")
                                .Append(smsconfig.Username).Append("&subacctpwd=").Append(smsconfig.Password)
                                .Append("&message=").Append(sms.Message).Append("&sender=").Append(smsconfig.SenderId)
                                .Append("&sendto=").Append(sms.Destination).Append("&msgtype=0");
                            var json = await httpClient.GetStringAsync(sb.ToString());
                            //var smsresponse = JsonConvert.DeserializeObject<SmsResponse>(json);
                            smslog.Response = json;
                            model[i].Status = "Completed"; smslog.ScheduleId = model[i].ScheduleId;
                            _context.Smslog.Add(smslog);
                            _context.Entry(model[i]).State = EntityState.Modified;
                            await _context.SaveChangesAsync();
                            //return Ok(new { ResponseCode = smsresponse.Code, Message = smsresponse.Message });
                        }
                        catch (Exception e)
                        {
                            return Content(e.Message);
                        }
                    }
                    _context.Entry(model[i]).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
            }
            return Ok(new { Status = "OK", Message = "Successfully Sent" });
        }

        public class SmsModel: BaseModel
        {
            public string Destination { get; set; }
            public string Message { get; set; }
        }

        public class SmsResponse
        {
            public int Code { get; set; }
            public string Message { get; set; }
        }
    }
}