﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/SmsApi")]
    public class SmsApiController : Controller
    {
        private readonly CmsDbContext _context;

        public SmsApiController(CmsDbContext context)
        {
            _context = context;
        }

        // GET: api/SmsApi
        [HttpGet]
        public IEnumerable<SmsApi> GetSmsApi()
        {
            return _context.SmsApi;
        }

        // GET: api/SmsApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSmsApi([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var smsApi = await _context.SmsApi.SingleOrDefaultAsync(m => m.SmsApiId == id);

            if (smsApi == null)
            {
                return NotFound();
            }

            return Ok(smsApi);
        }

        // GET: api/PaymentMethod/Company/5
        [HttpGet("Church/{id}")]
        public async Task<IActionResult> GetChurchSmsApi([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var smsapi = _context.SmsApi.Where(m => m.CompanyId == id);
            if (smsapi == null)
            {
                return NotFound();
            }

            return Ok(smsapi);
        }

        // GET: api/PaymentMethod/Branch/5
        [HttpGet("Branch/{id}")]
        public async Task<IActionResult> GetSmsApibyBranchId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var smsapi = _context.SmsApi.Where(m => m.BranchId == id);
            if (smsapi == null)
            {
                return NotFound();
            }
            return Ok(smsapi);
        }

        // PUT: api/SmsApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSmsApi([FromRoute] int id, [FromBody] SmsApi smsApi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != smsApi.SmsApiId)
            {
                return BadRequest();
            }

            _context.Entry(smsApi).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SmsApiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SmsApi
        [HttpPost]
        public async Task<IActionResult> PostSmsApi([FromBody] SmsApi smsApi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SmsApi.Add(smsApi);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSmsApi", new { id = smsApi.SmsApiId }, smsApi);
        }

        // DELETE: api/SmsApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSmsApi([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var smsApi = await _context.SmsApi.SingleOrDefaultAsync(m => m.SmsApiId == id);
            if (smsApi == null)
            {
                return NotFound();
            }

            _context.SmsApi.Remove(smsApi);
            await _context.SaveChangesAsync();

            return Ok(smsApi);
        }

        private bool SmsApiExists(int id)
        {
            return _context.SmsApi.Any(e => e.SmsApiId == id);
        }
    }
}