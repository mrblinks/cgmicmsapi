﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CmsApi.Models;
using Microsoft.AspNetCore.Authorization;
using CmsApi.MyServices;

namespace CmsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/CompanyDetails")]
    public class CompanyDetailsController : Controller
    {
        private readonly CmsDbContext _context;
        SequenceCode seq;

        public CompanyDetailsController(CmsDbContext context)
        {
            _context = context;
            seq = new SequenceCode(context);
        }

        // GET: api/CompanyDetails
        [HttpGet]
        public IEnumerable<CompanyDetails> GetCompayDetails()
        {
            return _context.CompanyDetails;
        }

        [HttpGet("Licence")]
        public IEnumerable<LicenceLog> GetCompayLicence()
        {
            return _context.LicenceLog.Include(a => a.Branch).Include(a => a.Company);
        }

        // GET: api/CompanyDetails/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCompanyDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var companyDetails = await _context.CompanyDetails.SingleOrDefaultAsync(m => m.CompanyId == id);

            if (companyDetails == null)
            {
                return NotFound();
            }

            return Ok(companyDetails);
        }

        // PUT: api/CompanyDetails/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompanyDetails([FromRoute] int id, [FromBody] CompanyDetails companyDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != companyDetails.CompanyId)
            {
                return BadRequest();
            }

            _context.Entry(companyDetails).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/CompanyDetails/5
        [HttpPost("Licence")]
        public async Task<IActionResult> PutCompanyDetails([FromBody] LicenceLog licence)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            CompanyDetails company = await _context.CompanyDetails.FindAsync(licence.CompanyId);
            if (company.ExpiryDate < licence.Next)
            {
                company.ExpiryDate = licence.Next;
            }
            BranchDetails branch = await _context.BranchDetails.FindAsync(licence.BranchId);
            licence.Previous = branch.ExpiryDate;
            branch.ExpiryDate = licence.Next;
            _context.Entry(company).State = EntityState.Modified;
            _context.Entry(branch).State = EntityState.Modified;
            licence.CreatedDate = DateTime.UtcNow;
            _context.LicenceLog.Add(licence);
            await _context.SaveChangesAsync();

            return Ok("Licence Updated");
        }
        
        // POST: api/CompanyDetails
        [HttpPost]
        public async Task<IActionResult> PostCompanyDetails([FromBody] CompanyDetails companyDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string snCode = await seq.GetCode("Church");

            companyDetails.CompanyNo = snCode;
            companyDetails.CreatedDate = DateTime.UtcNow;
            _context.CompanyDetails.Add(companyDetails);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompanyDetails", new { id = companyDetails.CompanyId }, companyDetails);
        }

        // DELETE: api/CompanyDetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCompanyDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var companyDetails = await _context.CompanyDetails.SingleOrDefaultAsync(m => m.CompanyId == id);
            if (companyDetails == null)
            {
                return NotFound();
            }

            _context.CompanyDetails.Remove(companyDetails);
            await _context.SaveChangesAsync();

            return Ok(companyDetails);
        }

        private bool CompanyDetailsExists(int id)
        {
            return _context.CompanyDetails.Any(e => e.CompanyId == id);
        }

    }
}