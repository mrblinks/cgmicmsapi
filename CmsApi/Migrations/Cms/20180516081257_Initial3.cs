﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CmsApi.Migrations.Cms
{
    public partial class Initial3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Code",
                table: "Smslog",
                newName: "ScheduleId");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Schedule",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Smslog_ScheduleId",
                table: "Smslog",
                column: "ScheduleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Smslog_Schedule_ScheduleId",
                table: "Smslog",
                column: "ScheduleId",
                principalTable: "Schedule",
                principalColumn: "ScheduleId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Smslog_Schedule_ScheduleId",
                table: "Smslog");

            migrationBuilder.DropIndex(
                name: "IX_Smslog_ScheduleId",
                table: "Smslog");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Schedule");

            migrationBuilder.RenameColumn(
                name: "ScheduleId",
                table: "Smslog",
                newName: "Code");
        }
    }
}
