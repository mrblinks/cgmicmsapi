﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using CmsApi.Models.Identity;
using Microsoft.AspNetCore.Authorization;

namespace CmsApi.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public ConfirmEmailModel(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public string Message { get; set; }

        public async Task<IActionResult> OnGet(string userId, string code)
        {
            if (userId == null || code == null)
            {
                Message = "User Id and code not supplied";
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                Message = ($"Unable to load user with ID '{userId}'.");
            }
            else
            {
                var result = await _userManager.ConfirmEmailAsync(user, code);
                if (!result.Succeeded)
                {
                    Message = ($"Error confirming email for user with ID '{userId}':");
                }
            }
            
            Message = $"User with email : {user?.UserName} confirmed successfully";
            Response.Redirect(location: "http://cms.cgmibzv.com/"); //cms.cgmibzv.com
            return null;
        }
    }
}