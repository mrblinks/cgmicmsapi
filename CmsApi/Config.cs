﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CmsApi
{
    public class Config
    {
        private static List<string> ConcurrentUsers = new List<string>();

        public static int? GetConcurrentUsers()
        {
            return ConcurrentUsers.Count;
        }

        public static bool AddConcurrentUsers(string cuser)
        {
            if (ConcurrentUsers.Any())
            {
                var user = ConcurrentUsers.FirstOrDefault(a => a == cuser);
                if (user != null)
                {
                    return false;
                }
                ConcurrentUsers.Add(cuser);
                return true;
            }
            else
            {
                ConcurrentUsers.Add(cuser);
                return true;
            }
        }

        public static bool RemoveConcurrentUsers(string cuser)
        {
            if (ConcurrentUsers.Any())
            {
                var user = ConcurrentUsers.FirstOrDefault(a => a == cuser);
                if (user != null)
                {
                    ConcurrentUsers.Remove(cuser);
                    return true;
                }
            }
            return false;
        }
    }
}
